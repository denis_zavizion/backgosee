@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @role('project-manager')
                        Project Manager Panel
                        <br/>
                    @endrole
                    @role('web-developer')
                        Web Developer Panel
                        <br/>
                    @endrole

                    @permission('manage-users')
                        !!!!!!!1Project manage-users
                    <br/>
                    @endpermission
                    @permission('create-tasks')
                        !!!!!!!!Web create-tasks
                    <br/>
                    @endpermission

{{--                    You are logged in!--}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

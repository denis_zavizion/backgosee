<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class howdoi extends Model
{
    protected $table = 'howdoi';
    protected $primaryKey = 'id';
    protected $fillable = ['parent_id', 'title', 'description', 'status'];
}

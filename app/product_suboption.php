<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product_suboption extends Model
{
    protected $table = 'product_suboption';

//    не учитывать дату
    public $timestamps = false;

    protected $fillable = ['product_id', 'sub_option_id'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $primaryKey = 'id';

//    не учитывать дату
    public $timestamps = false;

    protected $fillable = ['comment', 'product_id', 'user_id', 'status'];
}

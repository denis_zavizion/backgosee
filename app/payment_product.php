<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment_product extends Model
{
    protected $table = 'payment_product';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'amount', 'paid_up', 'currency', 'country', 'cart', 'source', 'receipt_url', 'status', 'evt_id', 'ch_id'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $table = 'product_option';

//    не учитывать дату
    public $timestamps = false;

    protected $fillable = ['product_id', 'opt_id'];
}

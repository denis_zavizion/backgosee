<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Таблица, связанная с моделью Product.
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * Определение первичного ключа product_id, по умолчанию id.
     *
     * @var string
     */
    protected $primaryKey = 'product_id';

//    не учитывать дату
//    public $timestamps = false;

    protected $fillable = ['title', 'img', 'gallery', 'description', 'budget', 'year', 'milage', 'user_id', 'cat_id',
        'name', 'country', 'phone', 'town', 'allow_messenger', 'allow_phone', 'allow_receipt', 'feature', 'status'];
}

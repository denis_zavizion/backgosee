<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class followers extends Model
{
    protected $table = 'followers';
//    не учитывать дату
    public $timestamps = true;
    protected $fillable = ['from_user_id', 'to_user_id', 'description', 'tag', 'mew' ,'url'];
}

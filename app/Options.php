<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    protected $table = 'options';
    protected $primaryKey = 'opt_id';

//    не учитывать дату
    public $timestamps = false;

    protected $fillable = ['opt_name', 'filter_id', 'status'];
}

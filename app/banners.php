<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class banners extends Model
{
    /**
     * Таблица, связанная с моделью banners.
     *
     * @var string
     */
    protected $table = 'banners';

    /* Не учитывать дату */
    public $timestamps = false;

    protected $fillable = ['title', 'page', 'position'];
}

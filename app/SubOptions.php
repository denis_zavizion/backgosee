<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubOptions extends Model
{
    protected $table = 'sub_options';
    protected $primaryKey = 'sub_option_id';

//    не учитывать дату
    public $timestamps = false;

    protected $fillable = ['sub_opt_title', 'opt_id', 'status'];
}

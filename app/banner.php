<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class banner extends Model
{
    /**
     * Таблица, связанная с моделью banners.
     *
     * @var string
     */
    protected $table = 'banner';

    /* Не учитывать дату */
    public $timestamps = false;

    protected $fillable = ['type', 'page', 'src', 'utl', 'position', 'product_id', 'categori_id'];
}

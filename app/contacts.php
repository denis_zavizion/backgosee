<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contacts extends Model
{
    protected $table = 'contacts';
    protected $primaryKey = 'contact_id';
    protected $fillable = ['name', 'email', 'subject', 'message', 'status'];
}

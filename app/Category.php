<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Таблица, связанная с моделью Category.
     *
     * @var string
     */
    protected $table = 'category';

    /**
     * Определение первичного ключа cat_id, по умолчанию id.
     *
     * @var string
     */
    protected $primaryKey = 'cat_id';

//    не учитывать дату
//    public $timestamps = false;

    protected $fillable = ['cat_id', 'title', 'img', 'color', 'parent_cat_id', 'sort', 'status'];
}

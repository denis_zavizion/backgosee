<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    /**
     * Таблица, связанная с моделью Category.
     *
     * @var string
     */
    protected $table = 'filters';

    /**
     * Определение первичного ключа cat_id, по умолчанию id.
     *
     * @var string
     */
    protected $primaryKey = 'filter_id';

//    не учитывать дату
    public $timestamps = false;

    protected $fillable = ['filter_name', 'sort', 'cat_id', 'status', 'img', 'type'];
}

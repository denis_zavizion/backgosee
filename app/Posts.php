<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = 'posts';
    protected $primaryKey = 'post_id';
    protected $fillable = ['title', 'description', 'img', 'main_img', 'edit', 'trend', 'transport', 'shares', 'min_read', 'status', 'youtube'];
}

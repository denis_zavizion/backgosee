<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\User;
use Illuminate\Routing\Controller;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        dd(2344234);
        $user = User::find(1);

//        $user->giveRolesTo('project-manager');
//        $user->givePermissionsTo('manage-users');
//        $user->deleteRoles('project-manager');
//        dd($user->hasRole('web-developer')); //вернёт true
//        dd($user->hasRole('project-manager')); //вернёт false
//        dd($user->givePermissionsTo('manage-users')); //выдаём разрешение
//        dd($user->hasPermission('manage-users')); //вернёт true
//        dd($user->hasPermission('create-tasks')); //вернёт true
//        dd($user->hasPermissionTo('web-developer')); //вернёт true
//        dd($user->hasPermissionThroughRole('create-tasks')); //вернёт true
//        dd($user->hasPermissionThroughRole('manage-users')); //вернёт true
//        dd($user->hasPermissionTo('manage-users')); //вернёт true


        return view('home');
    }

    public static function test()
    {
        return '11';
//        Gate::allows('manage-users');
//        $user = User::find(5);
//        dd($user->hasRole('web-developer')); //вернёт true
//        dd($user->hasRole('project-manager')); //вернёт false
//        dd($user->givePermissionsTo('manage-users')); //выдаём разрешение
//        dd($user->hasPermission('manage-users')); //вернёт true
    }
}

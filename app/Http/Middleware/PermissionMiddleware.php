<?php

namespace App\Http\Middleware;

use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     * @param $request
     * @param Closure $next
     * @param $role
     * @param null $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = null)
    {
        if(!auth()->user()->hasPermissionTo($permission)) {
            abort(404);
        }
        return $next($request);
    }
}

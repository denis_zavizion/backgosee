<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class term_meta extends Model
{
    protected $table = 'term_meta';

//    не учитывать дату
    public $timestamps = false;

    protected $fillable = ['key', 'val'];
}

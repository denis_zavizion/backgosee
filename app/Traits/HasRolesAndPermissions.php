<?php

namespace App\Traits;

use App\Role;
use App\Permission;
use App\User;

trait HasRolesAndPermissions
{
    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class,'users_roles');
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'users_permissions');
    }

    /**
     * @param mixed ...$roles
     * @return bool
     */
    public function hasRole(... $roles ) {
        foreach ($roles as $role) {
            if ($this->roles->contains('slug', $role)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission)
    {
        return (bool) $this->permissions->where('slug', $permission)->count();
    }

//    /**
//     * @param $permission
//     * @return bool
//     */
//    public function hasPermissionTo($permission)
//    {
//        return $this->hasPermission($permission);
//    }

    /**
     * @param $permission
     * @return bool
     */

    public function hasPermissionThroughRole($permission)
    {
        foreach ($this->roles as $role){
            $res = $role
                ->join('roles_permissions', 'roles_permissions.role_id', '=', 'roles.id')
                ->join('permissions', 'permissions.id', '=', 'roles_permissions.permission_id')
                ->where('permissions.slug', $permission)
                ->count();

            if( (bool) $res) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $permission
     * @return bool
     */
    public function hasPermissionTo($permission)
    {

        return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission);
    }

    /**
     * @param array $permissions
     * @return mixed
     */
    public function getAllPermissions(array $permissions)
    {
        return Permission::whereIn('slug',$permissions)->get();
    }

    public function getAllRoles(array $roles)
    {
        return Role::whereIn('slug',$roles)->get();
    }

    /**
     * @param mixed ...$permissions
     * @return $this
     */
    public function givePermissionsTo(... $permissions)
    {
        $permissions = $this->getAllPermissions($permissions);

        if($permissions === null) {
            return $this;
        }
        $this->permissions()->saveMany($permissions);
        return $this;
    }
    public function giveRolesTo(... $roles)
    {
        $permissions = $this->getAllRoles($roles);

        if($permissions === null) {
            return $this;
        }
        $this->roles()->saveMany($permissions);
        return $this;
    }
    /**
     * @param mixed ...$permissions
     * @return $this
     */
    public function deletePermissions(... $permissions )
    {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);
        return $this;
    }

    public function deleteRoles(... $roles )
    {
        $roles = $this->getAllRoles($roles);
        $this->roles()->detach($roles);
        return $this;
    }

    /**
     * @param mixed ...$permissions
     * @return HasRolesAndPermissions
     */
    public function refreshPermissions(... $permissions )
    {
        $this->permissions()->detach();
        return $this->givePermissionsTo($permissions);
    }

    public function getRoles($id)
    {
        $role = Role::join('users_roles', 'users_roles.role_id', '=', 'roles.id')
        ->join('back_user', 'back_user.id', '=', 'users_roles.user_id')
        ->where('back_user.id', (int)$id)
        ->select('roles.id');
        return $role;
    }

    public function getPermissions($id)
    {
        $permission = Permission::join('users_permissions', 'users_permissions.permission_id', '=', 'permissions.id')
        ->join('back_user', 'back_user.id', '=', 'users_permissions.user_id')
        ->where('back_user.id', (int)$id)
        ->select('permissions.id');
        return $permission;
    }

}

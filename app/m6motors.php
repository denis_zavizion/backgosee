<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m6motors extends Model
{
    protected $table = 'm6motors';
    protected $primaryKey = 'id';
    protected $fillable = ['api_id', 'product_id', 'status', 'mass'];
}

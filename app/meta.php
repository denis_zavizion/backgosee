<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class meta extends Model
{
    protected $table = 'meta';
    protected $primaryKey = 'id';

//    не учитывать дату
    public $timestamps = false;

    protected $fillable = ['title', 'description', 'keywords', 'alias', 'alias_id'];
}

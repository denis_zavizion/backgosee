<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $table = 'events';
    protected $primaryKey = 'event_id';
    protected $fillable = ['title', 'locate', 'description', 'img', 'lat', 'lng', 'lifetime', 'status'];
}

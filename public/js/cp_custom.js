/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./Resources/assets/js/custom.js":
/*!***************************************!*\
  !*** ./Resources/assets/js/custom.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var callbacks = {};
var revcallbacks = {}; //$(document).ready(function(){

if ($("#fpopup").length) {
  // if we have a popup to show
  i = 0;
  $("div[id='fpopup']").each(function () {
    var currentHTML = $(this).html();
    $(this).attr('did', i);
    $(this).html('<div id="hover' + i + '"></div><div id="popup' + i + '"><div id="close' + i + '">X</div>\<div class="animated bounce">' + currentHTML + '</div></div>');
    callbacks[String(i)] = $(this).attr("callback");
    revcallbacks[$(this).attr("callback")] = String(i);
    i++;
  }); //alert(revcallbacks.toSource())

  for (var key in callbacks) {
    var sector = callbacks[key];

    (function (sec) {
      $(sec).on("click", function (e) {
        var index = revcallbacks[sec];
        $("#hover" + index).fadeIn();
        $("#popup" + index).fadeIn();
      });
    })(sector);
  }

  $("[id^='hover']").click(function () {
    var didid = $(this).parent().attr('did');
    $(this).fadeOut();
    $("#popup" + didid).fadeOut();
  });
  $("[id^='close']").click(function () {
    var didid = $(this).parents().eq(1).attr('did');
    $("#hover" + didid).fadeOut();
    $("#popup" + didid).fadeOut();
  }); //});
}

function genKey() {
  var key = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  for (var i = 0; i < 16; i++) {
    key += possible.charAt(Math.floor(Math.random() * possible.length));

    if (i % 4 === 0 && !(i in [0])) {
      key += '-';
    }
  } // move last char to first, a little hacky


  key = key.substr(1) + key.substr(0, 1);
  return key;
}

var ids = [];
var currentActive2 = '';
var children = document.getElementById('objects').children;
var childrenLength = children.length;

for (var i = 0; i < childrenLength; i++) {
  if (children[i].nodeName.toLowerCase() === 'div') {
    ids.push(children[i].getAttribute('repr'));
  }
}

$("#cright").on('click', function () {
  var l = ids.length;
  var i = ids.indexOf(document.getElementById('currentActive').value);
  var nextid = ids[(i + 1) % l];

  for (var i = 0; i < childrenLength; i++) {
    if (children[i].getAttribute('repr') == document.getElementById('currentActive').value) {
      children[i].style.display = "none";
    }

    if (children[i].getAttribute('repr') == nextid) {
      children[i].style.display = "block";
      currentActive2 = children[i].getAttribute('repr');
    }
  }

  document.getElementById('currentActive').value = currentActive2;
  currentActive2 = '';
});
$("#cleft").on('click', function () {
  var l = ids.length;
  var i = ids.indexOf(document.getElementById('currentActive').value);
  var previd = ids[(i + l - 1) % l];

  for (var i = 0; i < childrenLength; i++) {
    if (children[i].getAttribute('repr') == document.getElementById('currentActive').value) {
      children[i].style.display = "none";
    }

    if (children[i].getAttribute('repr') == previd) {
      children[i].style.display = "block";
      currentActive2 = children[i].getAttribute('repr');
    }
  }

  document.getElementById('currentActive').value = currentActive2;
  currentActive2 = '';
});
$('.autocomplete-suggestion').css('width', '300px');

/***/ }),

/***/ 1:
/*!*********************************************!*\
  !*** multi ./Resources/assets/js/custom.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/denis/Project/cp_gosee/Modules/Cp/Resources/assets/js/custom.js */"./Resources/assets/js/custom.js");


/***/ })

/******/ });
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateM6motorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m6motors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('api_id');
            $table->string('product_id')->nullable();
            $table->string('status');
            $table->text('mass')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m6motors');
    }
}

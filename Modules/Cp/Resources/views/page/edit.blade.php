@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container">
                    <div class="custom-responsive">
                        <form action="{{route('page.edit.save', $page->term_id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="title" placeholder="title" value="{{$page->title}}">
                            @if($page->type == 'string')
                                <input type="text" name="val" placeholder="val" value="{{$page->val}}">
                            @else
                                <textarea  name="val" style="height: 200px">{{$page->val}}</textarea>
                                <script type="text/javascript">CKEDITOR.replace('val', {filebrowserUploadMethod: 'form'});</script>
                            @endif
                            <div><button type="submit" class="btn btn-primary">Save</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Title</th>
                                <th>Edit</th>
                                <th>Trend</th>
                                <th>Shares</th>
                                <th><a href="{{route('posts')}}/?sort=Moto">Moto</a>/<a href="{{route('posts')}}/?sort=Cars">Cars</a></th>
                                <th>Min_read</th>
                                @permission('edit_post')<th></th>@endpermission
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    @permission('edit_post')
                                    <td>
                                        <form action="{{route('post_status', $post->post_id)}}" method="get">
                                            <label class="switch">
                                                <input class="status_post" type="checkbox" name="status" id="togBtn_{{$post->post_id}}" {{$post->status ? "value=0 checked" : "value=1"}}>
                                                <div class="slider round"></div>
                                            </label>
                                        </form>
                                    </td>
                                    @else
                                        <td>{{$post->status == 1 ? 'on': 'off'}}</td>
                                    @endpermission
                                    <td>@permission('edit_post')<a href="{{route('post_edit', $post->post_id)}}">@endpermission {{$post->title}} @permission('edit_post') </a> @endpermission</td>
                                    <td>{{$post->edit}}</td>
                                    <td>{{$post->trend}}</td>
                                    <td>{{$post->shares}}</td>
                                    <td>{{$post->transport}}</td>
                                    <td>{{$post->min_read}}</td>
                                    @permission('edit_post') <td><a href="{{route('remove', $post->post_id)}}">remove</a></td> @endpermission
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @include('cp::inc.pagination')
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

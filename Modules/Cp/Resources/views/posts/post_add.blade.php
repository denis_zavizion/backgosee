@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign">jhon@deo.com </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container">
                    <div class="custom-responsive">
                        <form action="{{route('post_add_save')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="title" placeholder="title">
                            <textarea  name="description" style="height: 200px"></textarea>

                            <div>
                                <input type="checkbox" id="edit" name="edit" value="1">
                                <label for="edit">Edit</label>
                            </div>
                            <div>
                                <input type="checkbox" id="trend" name="trend" value="1">
                                <label for="trend">Trend</label>
                            </div>
                            <div>
                                <input type="checkbox" id="review" name="review" value="1">
                                <label for="review">Review</label>
                            </div>

                            <div class="flex_posts check">
                                <label for="shares">shares</label>
                                <input id="shares" type="text" name="shares" placeholder="0">

                                <label for="min_read">Min read</label>
                                <input id="min_read" type="text" name="min_read" placeholder="6">
                            </div>

                            <div class="flex_posts">
                                <div class="block_posts_one">
                                    <input type="file" name="img">
                                    <input type="file" name="main_img">
                                </div>
                                <div class="block_posts_two">
                                    <div>
                                        <input type="radio" id="Cars" name="transport" value="Cars">
                                        <label for="Cars">Cars</label>
                                    </div>

                                    <div>
                                        <input type="radio" id="Moto" name="transport" value="Moto">
                                        <label for="Moto">Moto</label>
                                    </div>

                                    <input type="text" name="video" placeholder="url">
                                </div>
                            </div>
                            <div><button type="submit" class="btn btn-primary">Save</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

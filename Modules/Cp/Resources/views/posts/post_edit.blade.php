@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container">
                    <div class="custom-responsive">
                        <form action="{{route('post_edit_save', $posts->post_id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="title" placeholder="title" value="{{$posts->title}}">
                            <textarea  name="description" style="height: 200px">{{$posts->description}}</textarea>
                            <script type="text/javascript">
                                CKEDITOR.replace('description', {
                                    filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
                                    filebrowserUploadMethod: 'form'
                                });
                            </script>
                            <div>
                                <input type="checkbox" id="edit" name="edit" value="1" {{$posts->edit ? 'checked' : null}}>
                                <label for="edit">Edit</label>
                            </div>
                            <div>
                                <input type="checkbox" id="trend" name="trend" value="1"  {{$posts->trend ? 'checked' : null}}>
                                <label for="trend">Trend</label>
                            </div>
                            <div>
                                <input type="checkbox" id="review" name="review" value="1" {{$posts->review ? 'checked' : null}}>
                                <label for="review">Review</label>
                            </div>

                            <div class="flex_posts check">
                                <label for="shares">shares</label>
                                <input id="shares" type="text" name="shares" placeholder="0" value="{{$posts->shares}}">

                                <label for="min_read">Min read</label>
                                <input id="min_read" type="text" name="min_read" placeholder="6" value="{{$posts->min_read}}">
                            </div>

                            <div class="flex_posts">
                                <div class="block_posts_one">
                                    <input type="file" name="img">
                                    <div><img src="{{@env('URL_FRONT')}}{{$posts->img}}"></div>
                                    <input type="file" name="main_img">
                                    <div><img src="{{@env('URL_FRONT')}}{{$posts->main_img}}"></div>
                                </div>
                                <div class="block_posts_two">
                                    <div>
                                        <input type="radio" id="Cars" name="transport" value="Cars" {{$posts->transport == 'Cars' ? 'checked' : null}}>
                                        <label for="Cars">Cars</label>
                                    </div>
                                    <div>
                                        <input type="radio" id="Moto" name="transport" value="Moto" {{$posts->transport == 'Moto' ? 'checked' : null}}>
                                        <label for="Moto">Moto</label>
                                    </div>
                                    <input type="text" name="video" placeholder="url" value="{{$posts->youtube}}">
                                </div>
                            </div>
                            <div><button type="submit" class="btn btn-primary">Save</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

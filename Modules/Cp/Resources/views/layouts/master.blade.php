<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Control Panel</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/cp.css') }}">

    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
</head>
<body>
<ul id="slide-out" class="side-nav fixed z-depth-4">
    <li>
        <div class="userView">
            <div class="background"></div>
            <a href="#!user"><img class="circle" src="/img/avatar04.png"></a>
            <a href="#!name"><span class="white-text name">Welcome back,</span></a>
            <a href="#!email"><span class="white-text email">{{ Auth::user()->name }}</span></a>
        </div>
    </li>
    <li><a class="active" href="{{route('dashboard')}}"><i class="material-icons pink-item">dashboard</i>Dashboard</a></li>
    <li class="no-padding"><a href="{{route('filter.report')}}">Report ({{Modules\Cp\Http\Controllers\filter\CategoryController::countReportsAll()}})</a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header">Posts<i class="material-icons pink-item">arrow_drop_down</i></a>
                <div class="collapsible-body">
                    <ul>
                        <a href="{{route('posts')}}">List Posts</a>
                        @permission('edit_post')<a href="{{route('post_add')}}">Add Post</a>@endpermission
                    </ul>
                </div>
            </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                @permission('view_category')<a class="collapsible-header">Filters<i class="material-icons pink-item">arrow_drop_down</i></a>@endpermission
                <div class="collapsible-body">
                    <ul>
                        @permission('view_category')<a href="{{route('filter.category')}}">Category</a>@endpermission
                        @permission('edit_category')<a href="{{route('filter.category.add')}}">Add Category</a>@endpermission
                    </ul>
                </div>
            </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                @permission('view_payment')<a class="collapsible-header">Payment<i class="material-icons pink-item">arrow_drop_down</i></a>@endpermission
                <div class="collapsible-body">
                    <ul>
                        @permission('view_payment')<a href="{{route('payment.check')}}">Check</a>@endpermission
                        @permission('view_payment')<a href="{{route('payment.pending')}}">Pending</a>@endpermission
                        @permission('view_payment')<a href="{{route('payment.successful')}}">Successful</a>@endpermission
                        @permission('view_payment')<a href="{{route('payment.archive')}}">Archive</a>@endpermission
                    </ul>
                </div>
            </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                @permission('view_payment')<a class="collapsible-header">SEO<i class="material-icons pink-item">arrow_drop_down</i></a>@endpermission
                <div class="collapsible-body">
                    <ul>
                        <a href="{{route('cp.meta.page.list')}}">Page</a>
                        <a href="{{route('cp.meta.post.list', 1)}}">Post</a>
                        <a href="{{route('cp.meta.category.list')}}">Category</a>
                        <a href="{{route('cp.meta.product.list', 1)}}">Product</a>
                    </ul>
                </div>
            </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                @permission('view_payment')<a class="collapsible-header">Parser<i class="material-icons pink-item">arrow_drop_down</i></a>@endpermission
                <div class="collapsible-body">
                    <ul>
                        <a href="{{route('parser.m6motors')}}">m6motors</a>
{{--                        <a href="{{env('URL_BACK')}}/cp/parser/m6motors">Page</a>--}}
                    </ul>
                </div>
            </li>
        </ul>
    </li>

    <li><a class="active" href="{{route('banner.list')}}"><i class="material-icons pink-item">queue</i>List Baners</a></li>

    <li><a class="active" href="{{route('events.list')}}"><i class="material-icons pink-item">queue</i>List Events</a></li>

    <li><a class="active" href="{{route('howdoi.list')}}"><i class="material-icons pink-item">queue</i>List Howdoi</a></li>

    <li><a class="active" href="{{route('contact.list')}}"><i class="material-icons pink-item">queue</i>Contacts</a></li>

    <li><a class="active" href="{{route('page.list')}}"><i class="material-icons pink-item">queue</i>Page</a></li>


    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                @permission('view_payment')<a class="collapsible-header">Dealer<i class="material-icons pink-item">arrow_drop_down</i></a>@endpermission
                <div class="collapsible-body">
                    <ul>
                        @permission('view_payment')<a href="{{route('dealer.list')}}">Pending for Dealer</a>@endpermission
                        @permission('view_payment')<a href="{{route('dealer.list', [2,0])}}">Dealer</a>@endpermission
{{--                        @permission('view_payment')<a href="{{route('payment.successful')}}">Successful</a>@endpermission--}}
{{--                        @permission('view_payment')<a href="{{route('payment.archive')}}">Archive</a>@endpermission--}}
                    </ul>
                </div>
            </li>
        </ul>
    </li>


{{--    <li><a class="active" href="{{route('dealer.list')}}"><i class="material-icons pink-item">queue</i>Dealer</a></li>--}}

    <li class="nav-item dropdown">
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </li>
    <li>
        <div class="divider"></div>
    </li>
</ul>

    @yield('content')

{{-- Laravel Mix - JS File --}}
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © 2020 Проект на Тестовое задание.
        </div>
    </div>
</footer>
<!-- So this is basically a hack, until I come up with a better solution. autocomplete is overridden
in the materialize js file & I don't want that.
-->
<!-- Yo dawg, I heard you like hacks. So I hacked your hack. (moved the sidenav js up so it actually works) -->
{{--<script src="{{ mix('js/cp.js') }}"></script>--}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
<script>
    // Hide sideNav
    $('.button-collapse').sideNav({
        menuWidth: 300, // Default is 300
        edge: 'left', // Choose the horizontal origin
        closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true // Choose whether you can drag to open on touch screens
    });
    $(document).ready(function () {
        $('.tooltipped').tooltip({delay: 50});
    });
    $('select').material_select();
    $('.collapsible').collapsible();
</script>
    <div class="fixed-action-btn horizontal tooltipped" data-position="top" data-position="top" data-delay="50" data-tooltip="Quick Links">
        <a class="btn-floating btn-large red">
            <i class="large material-icons">mode_edit</i>
        </a>
        <ul>
            <li><a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Handbook" href="#"><i
                        class="material-icons">insert_chart</i></a></li>
            <li><a class="btn-floating yellow darken-1 tooltipped" data-position="top" data-delay="50"
                   data-tooltip="Staff Applications" href="#"><i class="material-icons">format_quote</i></a></li>
            <li><a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Name Guidelines"
                   href="#"><i class="material-icons">publish</i></a></li>

            <li><a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Issue Tracker"
                   href="#"><i class="material-icons">attach_file</i></a></li>
            <li><a class="btn-floating orange tooltipped" data-position="top" data-delay="50" data-tooltip="Support" href="#"><i
                        class="material-icons">person</i></a></li>
        </ul>
    </div>
{{--</div>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--}}
{{--<script src="{{ mix('js/cp_custom.js') }}"></script>--}}
<script>$(".status_post").on('click', function() {$(this.form).submit();});</script>

</body>
</html>

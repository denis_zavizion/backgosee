@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1>
            </div>
            <div class="row">
                <form method="POST" action="{{route('parser.m6motors.nonce')}}">
                    @csrf
                    <input type="text" id="m6nonce" class="m6nonce" name="nonce">
                    <input type="submit" class="btn green" value="save">
                </form>
            </div>
        </section>
    </main>
@endsection

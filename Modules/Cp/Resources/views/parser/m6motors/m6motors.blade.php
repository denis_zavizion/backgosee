@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1>
            </div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                                <tr>
                                    <th>product</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($m6motors as $res)
                                {{Modules\Cp\Http\Controllers\ParserController::mass($res->api_id, $res->mass)}}
                            @endforeach
                            @foreach($added as $res)
                                <tr>
                                    <td><a href="{{ env('URL_FRONT') }}/product/{{$res->product_id}}/">{{$res->title}}</a></td>
                                    <td>
                                        <form action="{{route('m6motors.status', $res->id)}}" method="get">
                                            <label class="switch">
                                                <input class="status_post" type="checkbox" name="status" id="togBtn_{{$res->id}}" {{$res->status == 'added' ? "value='added'" : "value=1 checked"}}>
                                                <div class="slider round"></div>
                                            </label>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="btn_parser"><a href="{{route('parser.m6motors')}}">Try again</a></div>
        </section>
    </main>
@endsection

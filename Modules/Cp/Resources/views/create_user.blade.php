@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper"><a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i class="material-icons">menu</i></a><h1 class="page-announce-text valign"></h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <h3>Создать Пользователя !</h3>
                    <br>
                    <form action="{{route('cp.user.add.post')}}" method="post">
                        @csrf
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <input type="hidden" name="pastdata" value="" />
                                <td><label for="name">Username: </label></td>

                                <td>
                                    <i>@if ($errors -> has("name"))<span class="help-block"><strong>{{ $errors -> first("name") }}</strong></span>@endif</i>
                                    <input type="text" name="name" placeholder="User" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="email">Email: </label></td>
                                <td>
                                    @if ($errors -> has("email"))<span class="help-block"><strong>{{ $errors -> first("email") }}</strong></span>@endif
                                    <input type="text" name="email" placeholder="User@gmail.com" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="password">Password: </label></td>
                                <td>
                                    @if ($errors -> has("password"))<span class="help-block"><strong>{{ $errors -> first("password") }}</strong></span>@endif
                                    <input type="password" name="password" placeholder="*****" />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <div class="right-align"><input class="btn btn-success" type="submit" value="Submit" /></div>
                    </form>
                </div>
            </div>
        </section>
    </main>
@endsection

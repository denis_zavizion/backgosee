@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign">jhon@deo.com </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>Ролли</th>
                                @permission('edit_permission')<th></th>@endpermission
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($roles_all as $permission)
                                <tr>
                                    <td>{{$permission->name}}</td>
                                    @permission('edit_permission')
                                    <td>
                                        @if(in_array($permission->id, $roles))
                                            <a href="{{route('cp.role.update.remove', [$role_id, $permission->id])}}" class="btn red"><i class="material-icons">remove</i></a>
                                        @else
                                            <a href="{{route('cp.role.update.add', [$role_id, $permission->id])}}" class="btn green">add</a>
                                        @endif
                                    </td>
                                    @endpermission
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

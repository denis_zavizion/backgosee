@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>amount</th>
                                <th>paid_up</th>
                                <th>currency</th>
                                <th>cart</th>
                                <th>source</th>
                                <th>receipt_url</th>
                                <th>status</th>
                                <th>evt_id</th>
                                <th>ch_id</th>
                                <th>updated_at</th>
                                @permission('edit_post')<th></th>@endpermission
                                @permission('edit_post')<th></th>@endpermission
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{$payment->amount}}</td>
                                    <td>{{$payment->paid_up}}</td>
                                    <td>{{$payment->currency}}</td>
                                    <td>{{$payment->cart}}</td>
                                    <td>{{$payment->source}}</td>
                                    <td>{{$payment->receipt_url}}</td>
                                    <td>{{$payment->status}}</td>
                                    <td>{{$payment->evt_id}}</td>
                                    <td>{{$payment->ch_id}}</td>
                                    <td>{{$payment->updated_at}}</td>
                                    @permission('edit_post')<td><a href="{{route('payment.archive.save', $payment->id)}}" class="btn red"><i class="material-icons">remove</i></a></td>@endpermission
                                    @permission('edit_post')<td><a href="{{route('payment.successful.save', $payment->id)}}" class="btn green">access</a></td>@endpermission
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @include('cp::inc.pagination')
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

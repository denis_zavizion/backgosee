@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th></th>
                                <th>name</th>
                                <th>email</th>
                                <th>company</th>
                                <th>first_name</th>
                                <th>second_name</th>
                                <th>link</th>
                                <th>headerphoto</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dealers as $dealer)
                                <tr>

                                    <td>
                                        <form action="{{route('dealer.status', $dealer->id)}}" method="post">
                                            @csrf
                                            <label class="switch">
                                                <input class="status_post" type="checkbox" name="account" id="togBtn_{{$dealer->id}}" {{$dealer->account == 2 ? "value=1" : "value=2"}}>
                                                <div class="slider round"></div>
                                            </label>
                                        </form>
                                    </td>

                                    <td><a href="{{@env('URL_FRONT')}}/account/{{$dealer->id}}/allads">{{$dealer->name}}</a></td>
                                    <td>{{$dealer->email}}</td>
                                    <td>{{$dealer->company}}</td>
                                    <td>{{$dealer->first_name}}</td>
                                    <td>{{$dealer->second_name}}</td>
                                    <td><a href="{{$dealer->link}}">{{$dealer->link}}</a></td>
                                    <td><img style="width: 50px" src="{{@env('URL_FRONT')}}/{{$dealer->headerphoto}}"></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper"><a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i class="material-icons">menu</i></a><h1 class="page-announce-text valign">// Dashboard </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="col l3 s6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>420</h3>
                            <p>Accounts</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer" class="animsition-link">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div><!-- ./col -->
                <div class="col l3 s6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>69</h3>
                            <p>New Toons</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer" class="animsition-link">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div><!-- ./col -->
                <div class="col l3 s6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>36</h3>
                            <p>Support Emails</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-email"></i>
                        </div>
                        <a href="#" class="small-box-footer" class="animsition-link">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div><!-- ./col -->
                <div class="col l3 s6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>1337</h3>
                            <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer" class="animsition-link">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="container">
                    <div class="quick-links center-align">
                        <div class="row">
                            @permission('update_permission_role')
                            <div class="col l6 s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Mod Handbook"><a class="waves-effect waves-light btn-large" href="{{route('cp.role.list')}}">Setting permission Role</a></div>
                            <div class="col l6 s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Staff Applications"><a class="waves-effect waves-light btn-large" href="{{route('cp.role.create')}}">Create Role</a></div>
                            @endpermission
                            @permission('add_remov_user')
                                <div class="col l6 offset-l4 s12 tooltipped" data-position="top" data-delay="50" data-tooltip="OTRS Support Site"><a class="waves-effect waves-light btn-large" href="{{route('cp.user.add')}}">Add user</a></div>
                            @endpermission
                        </div>
                    </div>

                    <h3 class="center-align">List Users</h3>
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead><tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Date</th>
                                <th>Access</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->updated_at}}</td>
                                <td>
                                    @permission('edit_permission')<a href="{{route('cp.user', $user->id)}}" class="btn green">access</a>@endpermission
                                    @permission('add_remov_user')
                                        <a href="{{route('cp.user.remove', $user->id)}}" class="btn red"><i class="material-icons">remove</i></a>
                                    @endpermission
                                    @permission('edit_user')
                                        <a href="{{route('cp.user.edit', $user->id)}}" class="btn yellow">Edit</a>
                                    @endpermission
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>


{{--    @role('project-manager')--}}
{{--    Project Manager Panel--}}
{{--    <br/>--}}
{{--    @endrole--}}
{{--    @role('web-developer')--}}
{{--    Web Developer Panel--}}
{{--    <br/>--}}
{{--    @endrole--}}

{{--    @permission('manage-users')--}}
{{--    !!!!!!!1Project manage-users--}}
{{--    <br/>--}}
{{--    @endpermission--}}
{{--    @permission('create-tasks')--}}
{{--    !!!!!!!!Web create-tasks--}}
{{--    <br/>--}}
{{--    @endpermission--}}

@endsection

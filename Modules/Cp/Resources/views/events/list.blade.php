@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th></th>
                                <th>title</th>
                                <th>locate</th>
                                <th>description</th>
                                <th>lat</th>
                                <th>lng</th>
                                <th>img</th>
                                <th>shares</th>
                                <th>min_read</th>
                                <th>status</th>
                                <th>life time</th>
                                @permission('edit_events')<th></th>@endpermission
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($events as $event)
                                <tr>
                                    @permission('edit_events')
                                        <td>
                                            <form action="{{route('events.status', $event->event_id)}}" method="get">
                                                <label class="switch">
                                                    <input class="status_post" type="checkbox" name="status" id="togBtn_{{$event->event_id}}" {{$event->status == 1 ? "value='panding' checked" : "value=1"}}>
                                                    <div class="slider round"></div>
                                                </label>
                                            </form>
                                        </td>
                                    @else
                                        <td>{{$event->status == 1 ? 'on': 'off'}}</td>
                                    @endpermission
                                    <td>{{$event->title}}</td>
                                    <td>{{$event->locate}}</td>
                                    <td>{{$event->description}}</td>
                                    <td>{{$event->lat}}</td>
                                    <td>{{$event->lng}}</td>
                                    <td><img src="{{@env('URL_FRONT')}}/images/events/{{$event->event_id}}/{{$event->img}}" style="width: 50px"></td>
                                    <td>{{$event->shares}}</td>
                                    <td>{{$event->min_read}}</td>
                                    <td>{{$event->status}}</td>
                                    <td>{{$event->lifetime}}</td>
                                    @permission('edit_events')<td><a href="{{route('events.remove', $event->event_id)}}" class="btn red"><i class="material-icons">remove</i></a></td>@endpermission
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

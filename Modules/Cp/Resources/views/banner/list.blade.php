@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>title</th>
                                <th>page</th>
                                <th>position</th>
                                @permission('edit_banner')<th></th>@endpermission
                                @permission('view_banner')<th></th>@endpermission
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($banners as $banner)
                                <tr>
                                    <td>{{$banner->title}}</td>
                                    <td>{{$banner->page}}</td>
                                    <td>{{$banner->position}}</td>
                                    @permission('edit_banner')<td><a href="{{route('banner.list.edit', $banner->id)}}" class="btn red"><i class="material-icons">edit</i></a></td>@endpermission
                                    @permission('view_banner')<td><a href="{{route('banner.sidebar', [$banner->page, $banner->position])}}" class="btn green">next</a></td>@endpermission
                                    <td>{{Modules\Cp\Http\Controllers\BannerController::getCountBanner($banner->page, $banner->position)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

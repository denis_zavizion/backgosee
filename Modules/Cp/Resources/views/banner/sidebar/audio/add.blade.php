@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container">
                    <div class="custom-responsive">
                        <form action="{{route('banner.sidebar.save', [$page, $position])}}" method="post" enctype="multipart/form-data">
                            @csrf

                            <input name="type" type="hidden" value="audio">

                            <label for="img">file</label>
                            <input id="img" type="file" name="img">
                            <br/>
                            <br/>

                            <label for="post_id">post id</label>
                            <input id="post_id" type="number" name="categori_id" value="">

                            <div><button type="submit" class="btn btn-primary">Save</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

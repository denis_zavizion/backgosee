@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="quick-links center-align">
                        <div class="row">
                            <div class="col {{$page == 'post' ? 'l4' : 'l6'}} s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Mod Handbook" data-tooltip-id="311448ff-8867-9e74-0eae-ec2ae4020e2a"><a class="waves-effect waves-light btn-large" href="{{route('banner.sidebar.banner.add', [$page, $position])}}">Add Banner</a></div>
                            <div class="col {{$page == 'post' ? 'l4' : 'l6'}} s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Staff Applications" data-tooltip-id="a928ba34-0aed-4f72-3947-45db5e6f26b4"><a class="waves-effect waves-light btn-large" href="{{route('banner.sidebar.product.add', [$page, $position])}}">Add Product</a></div>
                            @if($page == 'post')<div class="col l4 s12 tooltipped" data-position="top" data-delay="50" data-tooltip="OTRS Support Site" data-tooltip-id="83fbaf9b-a9d7-f562-3c71-e6c3a499fb1b"><a class="waves-effect waves-light btn-large" href="{{route('banner.sidebar.audio.add', [$page, $position])}}">Add Audio</a></div>@endif
                        </div>
                    </div>

                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>type</th>
                                <th>page</th>
                                <th>src</th>
                                <th>url</th>
                                <th>position</th>
                                <th>product</th>
                                <th>cat/post</th>
                                @permission('edit_banner')<th></th>@endpermission
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($banners as $banner)
                                <tr>
                                    <td>{{$banner->type}}</td>
                                    <td>{{$banner->page}}</td>
                                    <td>@if($banner->type == 'banner')<img style="width: 50px" src="{{@env('URL_FRONT')}}{{$banner->src}}">@endif</td>
                                    <td>{{$banner->url}}</td>
                                    <td>{{$banner->position}}</td>
                                    <td>@if(isset($banner->product_id)){{Modules\Cp\Http\Controllers\BannerController::getProduct($banner->product_id)}} (№ {{$banner->product_id}})@endif</td>
                                    <td>@if(isset($banner->categori_id)){{$banner->page}} : {{Modules\Cp\Http\Controllers\BannerController::getCatPost($banner->page, $banner->categori_id)}} (№ {{$banner->categori_id}})
                                        @else
                                            all post
                                        @endif
                                    </td>
                                    <td>@permission('edit_banner')<a href="{{route('banner.sidebar.remove', $banner->id)}}" class="btn red"><i class="material-icons">remove</i></a>@endpermission </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

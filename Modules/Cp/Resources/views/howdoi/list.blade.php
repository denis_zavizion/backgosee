@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <a href="{{route('howdoi.add', 0)}}" class="btn green">add</a>
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th></th>
                                <th>title</th>
{{--                                <th>description</th>--}}
                                @permission('edit_howdoi')<th></th>@endpermission
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($howdois as $howdoi)
                                <tr>
                                    @permission('edit_howdoi')
                                        <td>
                                            <form action="{{route('howdoi.status', $howdoi->id)}}" method="get">
                                                <label class="switch">
                                                    <input class="status_post" type="checkbox" name="status" id="togBtn_{{$howdoi->id}}" {{$howdoi->status ? "value=0 checked" : "value=1"}}>
                                                    <div class="slider round"></div>
                                                </label>
                                            </form>
                                        </td>
                                    @else
                                        <td>{{$howdoi->status == 1 ? 'on': 'off'}}</td>
                                    @endpermission
                                    @if($howdoi->parent_id == 0)
                                        <td><a href="{{route('howdoi.list', $howdoi->id)}}">{{$howdoi->title}}</a></td>
                                    @else
                                        <td>{{$howdoi->title}}</td>
                                    @endif
{{--                                    <td>{!!$howdoi->description!!}</td>--}}
                                    @permission('edit_howdoi')<td><a href="{{route('howdoi.remove', $howdoi->id)}}" class="btn red"><i class="material-icons">remove</i></a></td>@endpermission
                                    @permission('edit_howdoi')<td><a href="{{route('howdoi.edit', $howdoi->id)}}" class="btn red"><i class="material-icons">edit</i></a></td>@endpermission
                                    @permission('edit_howdoi')
                                        <td>
                                            @if($howdoi->parent_id == 0)
                                                <a href="{{route('howdoi.add', $howdoi->id)}}" class="btn green">add</a>
                                            @endif
                                        </td>
                                    @endpermission
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

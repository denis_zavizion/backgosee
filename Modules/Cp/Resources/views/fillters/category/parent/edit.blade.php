
@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container">
                    <div class="custom-responsive">
                        <form action="{{route('filter.parent.save', $id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="title" placeholder="Hatchback" value="{{$category->title}}">
                            <input type="text" name="color" placeholder="#6BF3FD" value="{{$category->color}}">
                            <input type="text" name="sort" placeholder="1" value="{{$category->sort}}">

                            <div class="flex_posts">
                                <div class="block_posts_one">
                                    <input type="file" name="img">
                                    <div><img src="{{@env('URL_FRONT')}}/img/categories/{{$category->img}}"></div>
                                </div>
                            </div>

                            <div><button type="submit" class="btn btn-primary">Save</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

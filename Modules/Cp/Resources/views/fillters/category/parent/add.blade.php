
@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container">
                    <div class="custom-responsive" style="height: 100%; position: absolute;">
                        <form action="{{route('filter.category.add.save')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="title" placeholder="Hatchback" value="">
                            <input type="text" name="color" placeholder="#6BF3FD" value="">
                            <input type="text" name="sort" placeholder="1" value="">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Example select</label>
                                <select name="cat_id" class="form-control form-control-lg" id="exampleFormControlSelect1">
                                    @foreach($categories as $category)
                                        <option value="{{$category->cat_id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="flex_posts">
                                <div class="block_posts_one">
                                    <input type="file" name="img">
                                </div>
                            </div>

                            <div><button type="submit" class="btn btn-primary">Save</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection


@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
{{--            <!-- Stat Boxes -->--}}
            <div class="row">
                <div class="container">
                    <div class="custom-responsive">
                        <form action="{{route('category.filter.add.save', $cat_id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="filter_name" placeholder="title">
                            <input type="text" name="sort" placeholder="sort">

                            <div class="flex_posts">
                                <div class="block_posts_one">
                                    <input type="file" name="img">
                                </div>
                            </div>
                            @include('cp::inc/type')
                            <div><button type="submit" class="btn btn-primary">Save</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection


@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Status advert</th>
                                    <th>Status report</th>
                                    <th>Comments</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($reports as $report)
                                <tr>
                                    <td>{{$report->title}}</td>
                                    <td>
                                        <form action="{{route('filter.parent.auto.status', $report->product_id)}}" method="get">
                                            <label class="switch">
                                                <input class="status_post" type="checkbox" name="status" id="product" {{$report->product_status ? "value=0 checked" : "value=1"}}>
                                                <div class="slider round"></div>
                                            </label>
                                        </form>
                                    </td>
                                    <td style="width: 50px;">
                                        <form action="{{route('filter.parent.report', $report->id)}}" method="get">
                                            <label class="switch">
                                                <input class="status_post" type="checkbox" name="report" id="togBtn_{{$report->id}}" {{$report->status ? "value=0 checked" : "value=1"}}>
                                                <div class="slider round"></div>
                                            </label>
                                        </form>
                                    </td>
                                    <td>{{$report->comment}}</td>
                                    <td><a href="{{@env('URL_FRONT')}}/product/{{$report->product_id}}">link</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    @permission('edit_filter')<div class="plus_filter"><a href="{{route('category.filter.add', $id)}}">+</a></div>@endpermission
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Title</th>
                                <th>Sort</th>
                                <th>Status</th>
                                <th>Type</th>
                                @permission('edit_filter')<th></th>@endpermission
                                @permission('edit_filter')<th></th>@endpermission
{{--                                @permission('edit_category')<th></th>@endpermission--}}
{{--                                @permission('veiw_panel_filter')<th></th>@endpermission--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $filter)
                                <tr>
                                    @permission('edit_filter')
                                        <td>
                                            <form action="{{route('category.filter.status', $filter->filter_id)}}" method="get">
                                                <label class="switch">
                                                    <input class="status_post" type="checkbox" name="status" id="togBtn_{{$filter->filter_id}}" {{$filter->status ? "value=0 checked" : "value=1"}}>
                                                    <div class="slider round"></div>
                                                </label>
                                            </form>
                                        </td>
                                    @else
                                        <td>{{$filter->status == 1 ? 'on': 'off'}}</td>
                                    @endpermission
                                    <td>
                                        @permission('veiw_panel_options')
                                        @if(in_array($filter->type, ['sub_checkbox', 'checkbox', 'location', 'sale_wated']))
                                            <a href="{{route('option', $filter->filter_id)}}">{{$filter->filter_name}}</a>
                                        @else
                                            {{$filter->filter_name}}
                                        @endif
                                        @else
                                            {{$filter->filter_name}}
                                        @endpermission
                                    </td>
                                    <td>{{$filter->sort}}</td>
                                    <td>{{$filter->status}}</td>
                                    <td>{{$filter->type}}</td>
                                    @permission('edit_filter')<td><a href="{{route('category.filter.edit', $filter->filter_id)}}">Edit</a></td>@endpermission
                                    @permission('edit_filter')<td><a href="{{route('category.filter.delete', $filter->filter_id)}}">delete</a></td>@endpermission
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

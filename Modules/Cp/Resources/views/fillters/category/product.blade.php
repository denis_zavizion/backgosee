
@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <h3>Category {{$categories}}</h3>
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Title</th>
                                <th>Report</th>
                                <th>link</th>
                                <th>Special Deals</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    @permission('edit_category')
                                        <td>
                                            <form action="{{route('filter.parent.auto.status', $product->product_id)}}" method="get">
                                                <label class="switch">
                                                    <input class="status_post" type="checkbox" name="status" id="togBtn_{{$product->product_id}}" {{$product->status ? "value=0 checked" : "value=1"}}>
                                                    <div class="slider round"></div>
                                                </label>
                                            </form>
                                        </td>
                                    @else
                                        <td>{{$product->status == 1 ? 'on': 'off'}}</td>
                                    @endpermission
                                    <td>{{$product->title}}</td>
{{--                                        {{route('filter.parent', $product->cat_id)}}--}}
                                    <td><a href="{{route('report', $product->product_id)}}">{{Modules\Cp\Http\Controllers\filter\CategoryController::countReport($product->product_id)}}</a></td>
                                    <td><a href="{{@env('URL_FRONT')}}/product/{{$product->product_id}}">link</a></td>
                                    @permission('edit_category')
                                        <td>
                                            <form action="{{route('filter.parent.auto.special', $product->product_id)}}" method="get">
                                                <label class="switch">
                                                    <input class="status_post" type="checkbox" name="special" id="togBtnSpecial_{{$product->product_id}}" {{$product->special ? "value=0 checked" : "value=1"}}>
                                                    <div class="slider round"></div>
                                                </label>
                                            </form>
                                        </td>
                                    @else
                                        <td>{{$product->status == 1 ? 'on': 'off'}}</td>
                                    @endpermission
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
{{--                        @include('cp::inc.pagination')--}}
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

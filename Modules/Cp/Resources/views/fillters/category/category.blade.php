
@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Title</th>
                                <th>Img</th>
                                <th>Color</th>
                                <th>Sort</th>
                                @permission('edit_category')<th></th>@endpermission
                                @permission('veiw_panel_filter')<th></th>@endpermission
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    @permission('edit_category')
                                        <td>
                                            <form action="{{route('filter.category.cat_status', $category->cat_id)}}" method="get">
                                                <label class="switch">
                                                    <input class="status_post" type="checkbox" name="status" id="togBtn_{{$category->cat_id}}" {{$category->status ? "value=0 checked" : "value=1"}}>
                                                    <div class="slider round"></div>
                                                </label>
                                            </form>
                                        </td>
                                    @else
                                        <td>{{$category->status == 1 ? 'on': 'off'}}</td>
                                    @endpermission
                                    <td><a href="{{route('filter.parent', $category->cat_id)}}">{{$category->title}}</a></td>
                                    <td>{{$category->img}}</td>
                                    <td>{{$category->color}}</td>
                                    <td>{{$category->sort}}</td>
                                    @permission('edit_category')<td><a href="{{route('filter.parent.edit', $category->cat_id)}}">edit</a></td>@endpermission
                                    @permission('veiw_panel_filter')<td><a href="{{route('filter.list', $category->cat_id)}}">filter</a></td>@endpermission

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
{{--                        @include('cp::inc.pagination')--}}
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

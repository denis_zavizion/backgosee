
@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i class="material-icons">menu</i></a>
            </div>
{{--            <!-- Stat Boxes -->--}}
            <div class="row">
                <div class="container">
                    <div class="custom-responsive">
                        <form action="{{route('sub.option.add.save', $opt_id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="sub_opt_title" placeholder="title">
                            <div><button type="submit" class="btn btn-primary">Save</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="plus_filter"><a href="{{route('sub.option.add', $opt_id)}}">+</a></div>
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <tbody>
                            @foreach($options as $option)
                                <tr>
{{--                                    @permission('edit_category')--}}
                                    <td>
                                        <form action="{{route('sub.option.status', $option->sub_option_id)}}" method="get">
                                            <label class="switch">
                                                <input class="status_post" type="checkbox" name="status" id="togBtn_{{$option->sub_option_id}}" {{$option->status ? "value=0 checked" : "value=1"}}>
                                                <div class="slider round"></div>
                                            </label>
                                        </form>
                                    </td>
                                    <td>{{$option->sub_opt_title}}</td>
                                    @permission('edit_category')<td><a href="{{route('sub.option.edit', $option->sub_option_id)}}">Edit</a></td>@endpermission
                                    @permission('veiw_panel_filter')<td><a href="{{route('sub.option.delete', $option->sub_option_id)}}">delete</a></td>@endpermission
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

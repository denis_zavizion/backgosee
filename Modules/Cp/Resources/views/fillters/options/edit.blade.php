@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>

                <div class="row">
                    <div class="container">
                        <div class="custom-responsive">
                            <form action="{{route('option.save', [$option->opt_id, $option->filter_id])}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="text" name="opt_name" placeholder="title" value="{{$option->opt_name}}">
                                <div><button type="submit" class="btn btn-primary">Save</button></div>
                            </form>
                        </div>
                    </div>
                </div>
        </section>
    </main>
@endsection

@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    @permission('edit_options')<div class="plus_filter"><a href="{{route('option.add', $filter_id)}}">+</a></div>@endpermission
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <tbody>
                            @foreach($options as $option)
                                <tr>
                                    @permission('edit_options')
                                    <td>
                                        <form action="{{route('option.status', $option->opt_id)}}" method="get">
                                            <label class="switch">
                                                <input class="status_post" type="checkbox" name="status" id="togBtn_{{$option->opt_id}}" {{$option->status ? "value=0 checked" : "value=1"}}>
                                                <div class="slider round"></div>
                                            </label>
                                        </form>
                                    </td>
                                    @endpermission
                                    <td>
                                        @if(in_array($option->type, ['sub_checkbox']))
                                            <a href="{{route('suboption', $option->opt_id)}}">{{$option->opt_name}}</a>
                                        @else
                                            {{$option->opt_name}}
                                        @endif
                                    </td>
                                    @permission('edit_options')<td><a href="{{route('option.edit', $option->opt_id)}}">Edit</a></td>@endpermission
                                    @permission('edit_options')<td><a href="{{route('option.delete', $option->opt_id)}}">delete</a></td>@endpermission
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

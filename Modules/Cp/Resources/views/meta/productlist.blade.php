@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1>
            </div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>title</th>
                                <th>description</th>
                                <th>keywords</th>
                                <th>alias</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($meties as $meta)
                                <tr>
                                    @if(isset($meta->id))
                                        <td><a href="{{route('cp.meta.edit', [$meta->id, $n])}}">{{$meta->title}}</a></td>
                                        <td><a href="{{route('cp.meta.edit', [$meta->id, $n])}}">{{$meta->description}}</a></td>
                                        <td><a href="{{route('cp.meta.edit', [$meta->id, $n])}}">{{$meta->keywords}}</a></td>
                                        <td><a href="{{env('URL_FRONT')}}/{{$meta->alias}}/{{$meta->alias_id}}">{{$meta->alias}}</a></td>
                                    @else
                                        <td><a href="{{route('cp.meta.create.post',  ['product', $meta->product_id, $n])}}">{{$meta->ptitle}}</a></td>
                                        <td><a href=""></a></td>
                                        <td><a href=""></a></td>
                                        <td><a href="{{env('URL_FRONT')}}/product/{{$meta->product_id}}">{{$meta->ptitle}}</a></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation example" class="pagination">
                            <ul class="pagination">
                                @if($n > 1)
                                    <li class="page-item">
                                        <a class="page-link" href="{{route('cp.meta.product.list', $n-1)}}" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                @endif
                                @for ($i = $start; $i < $n; $i++)
                                    <li class="page-item"><a class="page-link" href="{{route('cp.meta.product.list', $i)}}">{{$i}}</a></li>
                                @endfor
                                @for ($i = $n; $i <= $end; $i++)
                                    <li class="page-item"><a class="page-link" href="{{route('cp.meta.product.list', $i)}}">{{$i}}</a></li>
                                @endfor
                                @if($n < $end)
                                    <li class="page-item">
                                        <a class="page-link" href="{{route('cp.meta.product.list', $n+1)}}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

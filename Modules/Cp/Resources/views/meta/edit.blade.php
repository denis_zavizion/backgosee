@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container">
                    <div class="custom-responsive">
                        <form action="{{route('cp.meta.save', $meta->id)}}" method="post">
                            @csrf
                            <input type="hidden" name="pag" placeholder="Title" value="{{$n}}">
                            <input type="text" name="title" placeholder="Title" value="{{$meta->title}}">
                            <input type="text" name="keywords" placeholder="Keyword" value="{{$meta->keywords}}">
                            <textarea  name="description" placeholder="Descriptions" style="height: 200px">{{$meta->description}}</textarea>
                            <div><button type="submit" class="btn btn-primary">Save</button></div>
                        </form>
                        <a href="{{$link}}">{{$link}}</a>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

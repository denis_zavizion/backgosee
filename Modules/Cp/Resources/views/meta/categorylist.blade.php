@extends('cp::layouts.master')

@section('content')
    <main>
        <section class="content">
            <div class="page-announce valign-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i
                        class="material-icons">menu</i></a>
                <h1 class="page-announce-text valign"> </h1></div>
            <!-- Stat Boxes -->
            <div class="row">
                <div class="container tb_mob">
                    <div class="custom-responsive">
                        <table class="striped hover centered">
                            <thead>
                            <tr>
                                <th>title</th>
                                <th>description</th>
                                <th>keywords</th>
                                <th>alias</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($meties as $meta)
                                <tr>
                                    @if(isset($meta->id))
                                        <td><a href="{{route('cp.meta.edit', $meta->id)}}">{{$meta->title}}</a></td>
                                        <td><a href="{{route('cp.meta.edit', $meta->id)}}">{{$meta->description}}</a></td>
                                        <td><a href="{{route('cp.meta.edit', $meta->id)}}">{{$meta->keywords}}</a></td>
                                        <td><a href="{{env('URL_FRONT')}}/{{$meta->alias}}/{{$meta->alias_id}}">{{$meta->alias}}</a></td>
                                    @else
                                        <td><a href="{{route('cp.meta.create.post',  ['category', $meta->cat_id])}}">{{$meta->ptitle}}</a></td>
                                        <td><a href=""></a></td>
                                        <td><a href=""></a></td>
                                        <td><a href="{{env('URL_FRONT')}}/post/{{$meta->cat_id}}">{{$meta->ptitle}}</a></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

<div class="form-group">
    <label for="exampleFormControlSelect1">Example select</label>
    <select name="type" class="form-control form-control-lg" id="exampleFormControlSelect1">
        <option value="sub_checkbox" {{isset($filter->type) && $filter->type == 'sub_checkbox' ? 'selected' : null}}>sub_checkbox</option>
        <option value="budget" {{isset($filter->type) && $filter->type == 'budget' ? 'selected' : null}}>budget</option>
        <option value="checkbox" {{isset($filter->type) && $filter->type == 'checkbox' ? 'selected' : null}}>checkbox</option>
        <option value="year" {{isset($filter->type) && $filter->type == 'year' ? 'selected' : null}}>year</option>
        <option value="milage" {{isset($filter->type) && $filter->type == 'milage' ? 'selected' : null}}>milage</option>
        <option value="location" {{isset($filter->type) && $filter->type == 'location' ? 'selected' : null}}>location</option>
        <option value="sale_wated" {{isset($filter->type) && $filter->type == 'sale_wated' ? 'selected' : null}}>sale_wated</option>

        <option value="engine" {{isset($filter->type) && $filter->type == 'engine' ? 'selected' : null}}>engine</option>
        <option value="tax" {{isset($filter->type) && $filter->type == 'tax' ? 'selected' : null}}>tax</option>
        <option value="fuel_economy" {{isset($filter->type) && $filter->type == 'fuel_economy' ? 'selected' : null}}>fuel_economy</option>
        <option value="nct_expiry" {{isset($filter->type) && $filter->type == 'nct_expiry' ? 'selected' : null}}>nct_expiry</option>
    </select>
</div>

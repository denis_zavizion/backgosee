<nav aria-label="Page navigation example" class="pagination">
    <ul class="pagination">
        @if($page['n'] > 1)
        <li class="page-item">
            <a class="page-link" href="{{route($page['route'])}}/?n={{$page['n'] - 1}}&sort={{$page['sort']}}" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        @endif
        @for ($i = $page['start']; $i < $page['n']; $i++)
            <li class="page-item"><a class="page-link" href="{{route($page['route'])}}/?n={{$i}}&sort={{$page['sort']}}">{{$i}}</a></li>
        @endfor
        @for ($i = $page['n']; $i <= $page['end']; $i++)
            <li class="page-item"><a class="page-link" href="{{route($page['route'])}}/?n={{$i}}&sort={{$page['sort']}}">{{$i}}</a></li>
        @endfor
        @if($page['n'] < $page['end'])
        <li class="page-item">
            <a class="page-link" href="{{route($page['route'])}}/?n={{$page['n'] + 1}}&sort={{$page['sort']}}" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
        @endif
    </ul>
</nav>

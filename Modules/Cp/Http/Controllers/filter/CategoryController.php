<?php


namespace Modules\Cp\Http\Controllers\filter;

use App\Category;
use App\Product;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cp\Http\Controllers\MailController;
use Hash;
use Validator;

class CategoryController extends Controller
{
    public function index(Request $request){
        $categories = Category::where('parent_cat_id', 0)->get();
        return view('cp::fillters.category.category', compact('categories'));
    }

    public function listAuto(Request $request, $id){
        $categories = Category::where('cat_id', $id)->pluck('title')->first();
        $products = product::where('cat_id', $id)->get();
        return view('cp::fillters.category.product', compact('products', 'categories'));
    }

    public function listAutoStatus(Request $request, $id){
        $products = product::find($id);
        $products->status = (int)$request->status;
        if ($request->status == 0){
            $product = Product::where('product_id', $id)->join('users', 'users.id', '=', 'product.user_id')->select('title', 'email')->first();
            MailController::ProductReportUsersFalse($product->title, $product->email);
        }else{
            $product = Product::where('product_id', $id)->join('users', 'users.id', '=', 'product.user_id')->select('title', 'email')->first();
            MailController::ProductReportUsersTrue($product->title, $product->email);
        }
        $products->save();
        return back();
    }

    public function reportStatus(Request $request, $id){
        $report = Report::find($id);
        $report->status = (int)$request->status;
        $report->save();
        return back();
    }

    public function listAutoSpecial(Request $request, $id){
        $products = product::find($id);
        $products->special = (int)$request->special;
        $products->save();
        $products = product::find($id);
        return back();
    }

    public function parent(Request $request, $id){
        $categories = Category::where('parent_cat_id', $id)->get();
        return view('cp::fillters.category.parent', compact('categories'));
    }

    public function delete(Request $request, $cat_id)
    {
        Category::where('cat_id', $cat_id)->delete();
        return back();
    }

    public function editParent(Request $request, $id){
        $category = Category::where('cat_id', $id)->first();
        return view('cp::fillters.category.parent.edit', compact('category', 'id'));
    }

    public function saveParent(Request $request, $cat_id){
        $url = $img = null;
        $category = Category::find($cat_id);
        $category->title = $request->title ? $request->title : 'title';
        $category->color = $request->color ? $request->color : '#6BF3FD';
        $category->sort = $request->sort ? $request->sort : 0;
        if(isset($request->img)) {
            if ($request->hasFile('img')) {
                $validator = $this->validator_img($request->all());
                if (!$validator->fails()) {
                    $file = $request->file('img');
                    $img = time() . '.' . $file->getClientOriginalName();
                    $url = '/cars/';
                    $file->move(env('PATH_URL_FRONT'). '/img/categories/' . $url, $img);
                    $category->img = $url.$img;
                }
            }
        }
        $category->save();
        return back();
    }

    public function addParent(Request $request){
        $categories = Category::where('parent_cat_id', 0)->get();
        return view('cp::fillters.category.parent.add', compact('categories'));
    }

    public function statusSaveParent(Request $request, $id){
        $post = Category::find($id);
        $post->status = (int)$request->status;
        $post->save();
        return back();
    }

    public function addSaveParent(Request $request){
        if(isset($request->img)) {
            if ($request->hasFile('img')) {
                $validator = $this->validator_img($request->all());
                if (!$validator->fails()) {
                    $file = $request->file('img');
                    $img = time() . '.' . $file->getClientOriginalName();
                    $url = '/cars/';
                    $file->move(env('PATH_URL_FRONT'). '/img/categories/' . $url, $img);
                    $src_img = $url.$img;
                }
            }
        }
        Category::create([
            'parent_cat_id' => $request->cat_id,
            'title' => $request->title,
            'color' => $request->color,
            'sort' => $request->sort,
            'img' => isset($src_img) ? $src_img: null
        ]);
        return redirect()->route('filter.category');
    }

    public function getReport($product_id){
        $product = product::where('product_id', $product_id)->first();
        $reports = Report::where('product_id', $product_id)->get();
        return view('cp::fillters.category.reports', compact('reports', 'product'));
    }

    public function filterReport(){
        $reports = Report::join('product', 'product.product_id', '=', 'reports.product_id')->where('reports.status', 1)
            ->select('product.product_id', 'product.title', 'product.special', 'reports.id', 'reports.comment', 'reports.status', 'product.status as product_status')->get();
        return view('cp::fillters.category.filterReports', compact('reports'));
    }

    private function validator_img(array $data){
        return Validator::make($data, [
            'img' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
    }

    public static function countReportsAll(){
        $reports = Report::where('status', 1)->count();
        return $reports;
    }

    public static function countReport($product_id){
        $reports = Report::where('product_id', $product_id)->where('status', 1)->count();
        return $reports;
    }
}

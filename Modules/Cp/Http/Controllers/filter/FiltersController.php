<?php


namespace Modules\Cp\Http\Controllers\filter;

use App\Filter;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Hash;
use Validator;

class FiltersController extends Controller
{
    public function list(Request $request, $id){
        $categories = Filter::where('cat_id', $id)->get();

        return view('cp::fillters.category.filter', compact('categories', 'id'));
    }

    public function delete(Request $request, $filter_id)
    {
        Filter::where('filter_id', $filter_id)->delete();
        return back();
    }

    public function edit(Request $request, $filter_id){
        $filter = Filter::where('filter_id', $filter_id)->first();
        return view('cp::fillters.category.edit', compact( 'filter', 'filter_id'));
    }

    public function add(Request $request, $cat_id){
        return view('cp::fillters.category.add', compact('cat_id'));
    }

    public function status(Request $request, $filter_id){
        $post = Filter::find($filter_id);
        $post->status = (int)$request->status;
        $post->save();

        return back();
    }

    public function save(Request $request, $filter_id){
        $url = $img = null;
        $filters = Filter::find($filter_id);
        $filters->filter_name = $request->filter_name ? $request->filter_name : 'title';
        $filters->sort = $request->sort ? $request->sort : 1;
        $filters->type = $request->type ? $request->type : 'checkbox';
        if(isset($request->img)) {
            if ($request->hasFile('img')) {
                $validator = $this->validator_img($request->all());
                if (!$validator->fails()) {
                    $file = $request->file('img');
                    $img = time() . '.' . $file->getClientOriginalName();

                    $file->move(env('PATH_URL_FRONT'). '/img/filter/', $img);
                    $filters->img = $img;
                }
            }
        }
        $filters->save();
        return back();
    }

    public function addSave(Request $request, $cat_id){
        if(isset($request->img)) {
            if ($request->hasFile('img')) {
                $validator = $this->validator_img($request->all());
                if (!$validator->fails()) {
                    $file = $request->file('img');
                    $img = time() . '.' . $file->getClientOriginalName();
                    $file->move(env('PATH_URL_FRONT'). '/img/filter/' , $img);
                    $src_img = $img;
                }
            }
        }
        Filter::create([
            'filter_name' => $request->filter_name,
            'sort' => $request->sort,
            'type' => $request->type,
            'cat_id' => $cat_id,
            'img' => isset($src_img) ? $src_img: null
        ]);
        return redirect()->route('filter.list', $cat_id);
    }

    private function validator_img(array $data){
        return Validator::make($data, [
            'img' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
    }
}

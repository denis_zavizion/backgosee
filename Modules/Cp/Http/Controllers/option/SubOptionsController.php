<?php


namespace Modules\Cp\Http\Controllers\option;

use App\SubOptions;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Hash;
use Validator;

class SubOptionsController extends Controller
{
    public function index(Request $request, $opt_id){
        $options = SubOptions::where('opt_id', $opt_id)->get();

        return view('cp::fillters.suboptions.index', compact('options', 'opt_id'));
    }

    public function delete(Request $request, $sub_option_id)
    {
        SubOptions::where('sub_option_id', $sub_option_id)->delete();
        return back();
    }

    public function edit(Request $request, $sub_option_id){

        $option = SubOptions::where('sub_option_id', $sub_option_id)->first();
        return view('cp::fillters.suboptions.edit', compact( 'option'));
    }

    public function add(Request $request, $opt_id){
        return view('cp::fillters.suboptions.add', compact('opt_id'));
    }

    public function status(Request $request, $sub_option_id){
        $post = SubOptions::find($sub_option_id);
        $post->status = (int)$request->status;
        $post->save();
        return back();
    }

    public function save(Request $request, $opt_id, $sub_option_id){
        $option = SubOptions::find($sub_option_id);
        $option->sub_opt_title = $request->sub_opt_title ? $request->sub_opt_title : null;
        $option->save();
        return redirect()->route('suboption', $opt_id);
    }

    public function addSave(Request $request, $opt_id){
        SubOptions::create([
            'sub_opt_title' => $request->sub_opt_title,
            'opt_id' => $opt_id,
            'status' => 1,
        ]);
        return redirect()->route('suboption', $opt_id);
    }

}

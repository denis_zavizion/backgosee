<?php


namespace Modules\Cp\Http\Controllers\option;

use App\Filter;
use App\Options;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Hash;
use Validator;

class OptionsController extends Controller
{
    public function index(Request $request, $filter_id){
        $options = Options::where('options.filter_id', $filter_id)
            ->join('filters', 'filters.filter_id', '=', 'options.filter_id')
            ->select('options.*', 'filters.type')
            ->get();

        return view('cp::fillters.options.index', compact('options', 'filter_id'));
    }

    public function delete(Request $request, $opt_id)
    {
        Options::where('opt_id', $opt_id)->delete();
        return back();
    }

    public function edit(Request $request, $opt_id){

        $option = Options::where('opt_id', $opt_id)->first();
        return view('cp::fillters.options.edit', compact( 'option'));
    }

    public function add(Request $request, $filter_id){
        return view('cp::fillters.options.add', compact('filter_id'));
    }

    public function status(Request $request, $opt_id){
        $post = Options::find($opt_id);
        $post->status = (int)$request->status;
        $post->save();
        return back();
    }

    public function save(Request $request, $opt_id, $filter_id){
        $option = Options::find($opt_id);
        $option->opt_name = $request->opt_name ? $request->opt_name : null;
        $option->save();
        return redirect()->route('option', $filter_id);
    }

    public function addSave(Request $request, $filter_id){
        Options::create([
            'opt_name' => $request->opt_name,
            'filter_id' => $filter_id,
            'status' => 1,
        ]);
        return redirect()->route('option', $filter_id);
    }

}

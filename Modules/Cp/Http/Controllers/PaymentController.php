<?php


namespace Modules\Cp\Http\Controllers;

use App\Product;
use App\payment_product;
use Illuminate\Http\Request;

class PaymentController
{
    protected $take = 10;

    public function check(Request $request){
        $p = $this->page($request, 'check', 'payment.check');
        return view('cp::payment.check', ['payments'=>$p['payments'], 'page'=>$p['page']]);
    }
    public function pending(Request $request){
        $p = $this->page($request, 'pending', 'payment.check');
        return view('cp::payment.check', ['payments'=>$p['payments'], 'page'=>$p['page']]);
    }
    public function successful(Request $request){
        $p = $this->page($request, 'success', 'payment.check');
        return view('cp::payment.check', ['payments'=>$p['payments'], 'page'=>$p['page']]);
    }
    public function archive(Request $request){
        $p = $this->page($request, 'archive', 'payment.check');
        return view('cp::payment.check', ['payments'=>$p['payments'], 'page'=>$p['page']]);
    }
    private function page($request, $status, $route){
        $request->sort ? $sort = $request->sort : $sort = 0;
        isset($request->n) ? $n = $request->n : $n = 1;
        $payments = payment_product::where('status', $status)->latest()->skip(($n-1)*$this->take)->take($this->take)->get();
        $page = $this->paginate(payment_product::where('status', $status)->count(), $n, $sort, $route);
        return ['payments'=>$payments, 'page'=>$page];
    }

    public function archiveSave(Request $request, $product_id){
        $payment = payment_product::find($product_id);
        $payment->status = 'archive';
        $payment->save();
        $product = Product::find($product_id);
        $product->status = 0;
        $product->save();
        return back();
    }
    public function successfulSave(Request $request, $product_id){
        $payment = payment_product::find($product_id);
        $payment->status = 'success';
        $payment->save();
        $product = Product::find($product_id);
        $product->status = 1;
        $product->save();
        return back();
    }
    public function stopSave(Request $request, $product_id){
        $product = Product::find($product_id);
        $product->status = 0;
        $product->save();
        return back();
    }
    private function paginate($count, $n, $sort, $route){
        $page = 1;
        if($count > $this->take){
            $page = ceil($count / $this->take);
        }
        $end = $page;
        $k = 3;
        if($n+$k <= $page){$end = $n+$k;}
        $start = 1;
        if($n-$k > 0){$start = $n-$k;}
        return ['page'=>$page, 'end'=>$end, 'n'=>$n, 'start'=>$start, 'sort'=>$sort, 'route'=>$route];
    }
}

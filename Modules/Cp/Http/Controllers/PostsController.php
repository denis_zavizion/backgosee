<?php


namespace Modules\Cp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Permission;
use App\Posts;
use Hash;
use Validator;

class PostsController extends Controller
{
    protected $take = 10;
    public function index(Request $request){
        isset($request->n) ? $n = $request->n : $n = 1;
        $request->sort ? $sort = $request->sort : $sort = 0;

        $posts = Posts::latest();
        $request->sort ? $posts = $posts->where('transport', $request->sort) : null;
        $posts = $posts->skip(($n-1)*$this->take)->take($this->take)->get();

        $page = $this->paginate(Posts::where('status', 1)->count(), $n, $sort, 'posts');
        return view('cp::posts.posts', compact('posts', 'page'));
    }

    public function statusPost(Request $request, $id){
        $post = Posts::find($id);
        $post->status = (int)$request->status;
        $post->save();
        return back();
    }

    public function edit(Request $request, $post_id){
        $posts = Posts::where('post_id', $post_id)->first();
        return view('cp::posts.post_edit', compact('posts'));
    }

    public function create(Request $request){
        return view('cp::posts.post_add');
    }

    public function save(Request $request, $post_id){
        $post = Posts::find($post_id);
        isset($request->title) ? $post->title = $request->title : null;
        isset($request->description) ? $post->description = $request->description : null;

        isset($request->edit) ? $post->edit = $request->edit : $post->edit = 0;
        isset($request->trend) ? $post->trend = $request->trend : $post->trend = 0;

        isset($request->shares) ? $post->shares = $request->shares : null;
        isset($request->min_read) ? $post->min_read = $request->min_read : null;
        isset($request->transport) ? $post->transport = $request->transport : null;

        if(isset($request->img)) {
            if ($request->hasFile('img')) {
                $validator = $this->validator_img($request->all());
                if (!$validator->fails()) {
                    $file = $request->file('img');

                    $img = time() . '.' . $file->getClientOriginalName();
                    $url = '/img/posts/';
                    $file->move(env('PATH_URL_FRONT') . $url, $img);
                    $post->img = $url.$img;;
                }
            }
        }

        if(isset($request->main_img)) {;
            if ($request->hasFile('main_img')) {
                $validator = $this->validator_main_img($request->all());
                if (!$validator->fails()) {
                    $file = $request->file('main_img');

                    $img = time() . '.' . $file->getClientOriginalName();
                    $url = '/img/post/';
                    $file->move(env('PATH_URL_FRONT') . $url, $img);
                    $post->main_img = $url.$img;
                }
            }
        }
        $video = '';
        if($request->video){
            $video = substr(strrchr($request->video, "/"), 1);
        }
        isset($video) ? $post->youtube = $video : $post->youtube = null;
        $post->review = $request->review;
        $post->save();
        return back();
    }

    public function addPostSave(Request $request){
        $url = '/images/posts/';
        $src_img = $src_main_img = null;
        if(isset($request->img)) {
            if ($request->hasFile('img')) {
                $validator = $this->validator_img($request->all());
                if (!$validator->fails()) {
                    $file = $request->file('img');
                    $img = time() . '.' . $file->getClientOriginalName();
                    $file->move(public_path() . $url, $img);
                    $src_img = $url.$img;;
                }
            }
        }
        if(isset($request->main_img)) {;
            if ($request->hasFile('main_img')) {
                $validator = $this->validator_main_img($request->all());
                if (!$validator->fails()) {
                    $file = $request->file('main_img');
                    $img = time() . '.' . $file->getClientOriginalName();
                    $file->move(public_path() . $url, $img);
                    $src_main_img = $url.$img;
                }
            }
        }

        $video = '';
        if($request->video){
            $video = substr(strrchr($request->video, "/"), 1);
        }


        $post = Posts::create([
            'title' => $request->title,
            'description' => $request->description,
            'edit' => isset($request->edit) ? $request->edit : '',
            'trend' => isset($request->trend) ? $request->trend : '',
            'review' => isset($request->review) ? $request->review : '',
            'shares' => isset($request->shares) ? $request->shares : 0,
            'min_read' =>isset($request->min_read) ? $request->min_read : 0,
            'transport' => $request->transport,
            'youtube' => $video,
            'img' => $src_img,
            'main_img' => $src_main_img
        ]);

        return redirect()->route('posts');
    }

    public function remove(Request $request, $post_id){
        Posts::find($post_id)->delete();
        return back();
    }

    private function paginate($count, $n, $sort, $route){
        $page = 1;
        if($count > $this->take){
            $page = ceil($count / $this->take);
        }

        $end = $page;
        $k = 3;
        if($n+$k <= $page){$end = $n+$k;}
        $start = 1;
        if($n-$k > 0){$start = $n-$k;}

        return ['page'=>$page, 'end'=>$end, 'n'=>$n, 'start'=>$start, 'sort'=>$sort, 'route'=>$route];
    }

    private function validator_img(array $data){
        return Validator::make($data, [
            'img' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
    }

    private function validator_main_img(array $data){
        return Validator::make($data, [
            'main_img' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
    }

}

<?php

namespace Modules\Cp\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use Modules\Cp\Entities\test1;
use Validator;

class CpController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('cp::index', ['users' => User::where('id', '<>' ,1)->get()]);
    }

    public function user(Request $request, $id)
    {
        $roles_all =  Role::all();
        $permissions_all = Permission::all();
        $roles = User::find($id)->getRoles($id)->pluck('id')->toArray();
        $permissions = User::find($id)->getPermissions($id)->pluck('id')->toArray();
        return view('cp::user', compact('roles', 'permissions', 'roles_all', 'permissions_all', 'id'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function createRoleUser(Request $request, $id, $role_slug)
    {
        User::find($id)->giveRolesTo($role_slug);
        return back();
    }

    public function removeRoleUser(Request $request, $id, $role_slug)
    {
        User::find($id)->deleteRoles($role_slug);
        return back();
    }

    public function createPermissionUser(Request $request, $id, $permission_slug)
    {
        User::find($id)->givePermissionsTo($permission_slug);
        return back();
    }

    public function removePermissionUser(Request $request, $id, $permission_slug)
    {
        User::find($id)->deletePermissions($permission_slug);
        return back();
    }


    public function createUser(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = $this->validator($request->all());
            if (!$validator->fails()) {
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->save();
                return redirect()->route('dashboard');
            }else{
                return redirect()->back()->withErrors($validator)->withInput();
            }
            return view('cp::create_user');
        }

        return view('cp::create_user');
    }

    public function removeUser(Request $request, $id)
    {
        User::find($id)->delete();
        return back();
    }

    public function editUser(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $validator = $this->validator_update($request->all());
            if (!$validator->fails()) {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->save();
                return redirect()->route('dashboard');
            }else{
                return redirect()->back()->withErrors($validator)->withInput();
            }
            return view('cp::create_user');
        }

        return view('cp::edit_user',['id'=>$id, 'user' => User::find($id)]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    protected function validator_update(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }
}

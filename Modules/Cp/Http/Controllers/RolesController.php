<?php

namespace Modules\Cp\Http\Controllers;

use App\Permission;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cp\Entities\test1;
use Validator;
use App\Role;
use App\RolesPermissions;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */


    public function createRole(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = $this->validator_create($request->all());
            if (!$validator->fails()) {
                $role = new Role();
                $role->name = $request->name;
                $role->slug = $request->slug;
                $role->save();
                return redirect()->route('dashboard');
            }else{
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }

        return view('cp::role.create_role');
    }

    public function listRole(Request $request)
    {
        $roles =  Role::all();
        return view('cp::role.list_role', ['roles' => $roles]);
    }

    public function removeRole(Request $request, $id)
    {
        Role::find($id)->delete();
        return back();
    }

    public function updateRoleAdd(Request $request, $role_id, $permission_id)
    {
        $RolesPermissions = new RolesPermissions();
        $RolesPermissions->role_id = $role_id;
        $RolesPermissions->permission_id = $permission_id;
        $RolesPermissions->save();
        return back();
    }

    public function updateRoleRemove(Request $request, $role_id, $permission_id)
    {
        RolesPermissions::where('role_id', $role_id)->where('permission_id', $permission_id)->delete();
        return back();
    }

    public function updateRole(Request $request, $role_id)
    {
        $roles = Role::
            join('roles_permissions', 'roles_permissions.role_id', '=', 'roles.id')
            ->join('permissions', 'permissions.id', '=', 'roles_permissions.permission_id')
            ->where('roles.id', $role_id)
            ->select('permissions.id')
            ->pluck('permissions.id')->toArray();

        return view('cp::role.update_role', ['role_id' => $role_id, 'roles_all'=> Permission::all(), 'roles' => $roles]);
    }


    protected function validator_create(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'slug' => ['required', 'string', 'max:15'],
        ]);
    }

}

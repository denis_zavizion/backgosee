<?php

namespace Modules\Cp\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public static function ProductReportUsersFalse($title, $email)
    {
        $arr = ['title' => $title, 'body' => 'Your advert '. $title .' has been suspended for review as it has been reported by another use'];
        Mail::send('cp::email.report', $arr, function($message) use ($title, $email) {
            $message->to($email, $title)->subject('Report this advert');
        });
    }
    public static function ProductReportUsersTrue($title, $email)
    {
        $arr = ['title' => $title, 'body' => 'Your advert '. $title .' is included.'];
        Mail::send('cp::email.report', $arr, function($message) use ($title, $email) {
            $message->to($email, $title)->subject('Report this advert');
        });
    }
}

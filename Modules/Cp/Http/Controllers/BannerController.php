<?php


namespace Modules\Cp\Http\Controllers;


use Illuminate\Http\Request;
use App\banners;
use App\banner;
use App\Category;
use App\Product;
use App\Posts;
use Illuminate\Routing\Controller;

class BannerController extends Controller
{
    public function index(Request $request){
        return view('cp::banner.list', ['banners'=>banners::all()]);
    }

    public function edit(Request $request, $id){
        return view('cp::banner.list_edit', ['banner'=>banners::find($id)]);
    }

    public function editSave(Request $request, $id){
        $banners = banners::find($id);
        $banners->title = isset($request->title) ? $request->title : null;
        $banners->save();
        return redirect()->route('banner.list');
    }

    public function sidebar(Request $request, $page, $position){
        $banners = banner::where('page', $page)->where('position', $position)->get();
        return view('cp::banner.sidebar', compact('banners', 'page', 'position'));
    }

    public function addSidebarBanner(Request $request, $page, $position){
        return view('cp::banner.sidebar.banner.add', compact('page', 'position'));
    }
    public function addSidebarAudio(Request $request, $page, $position){
        return view('cp::banner.sidebar.audio.add', compact('page', 'position'));
    }
    public function addSidebarProduct(Request $request, $page, $position){
        return view('cp::banner.sidebar.product.add', compact('page', 'position'));
    }

    public function saveSidebar(Request $request, $page, $position){
        if(isset($request->img)) {
            if ($request->hasFile('img')) {
                $file = $request->file('img');
                $img = time() . '.' . $file->getClientOriginalName();

                $url = '/img/'.$request->type.'/'.$page.'/';

                if(isset($request->product_id)){$url.= 'product/'.$request->product_id.'/';}
                if(isset($request->categori_id)){$url.= 'cat/'.$request->categori_id.'/';}

                $file->move(env('PATH_URL_FRONT'). $url, $img);
                $img = $url.$img;
            }
        }
        banner::create([
            'type' => $request->type,
            'src' => isset($img) ? $img : null,
            'url' => isset($request->link) ? (int)$request->link : null,
            'product_id' => isset($request->product_id) ? (int)$request->product_id : null,
            'categori_id' => isset($request->categori_id) ? (int)$request->categori_id : null,

            'page' => $page,
            'position' => $position
        ]);
        return redirect()->route('banner.sidebar', [$page, $position]);
    }

    public function removeSidebar($id){
        banner::find($id)->delete();
        return back();
    }

    public static function getProduct($product_id){
        $product = Product::find($product_id);
        if(isset($product) && $product)
            return view('cp::banner.inc.product', ['product'=>$product]);
        return null;
    }
    public static function getCountBanner($page, $position){
        $banner = banner::where('page', $page)->where('position', $position)->where('type', 'banner')->count();
        $audio = banner::where('page', $page)->where('position', $position)->where('type', 'audio')->count();
        $product = banner::where('page', $page)->where('position', $position)->where('type', 'product')->count();

        return view('cp::banner.inc.count', compact('banner', 'audio', 'product'));
    }
    public static function getCatPost($page, $id=null){
        if(isset($page) && isset($id)){
            if ($page == 'category'){
                $category = Category::find($id);
                return view('cp::banner.inc.category', ['category'=>$category]);
            }

            if ($page == 'post'){
                $post = Posts::find($id);
                return view('cp::banner.inc.post', ['post'=>$post]);
            }
        }
        return null;
    }
}

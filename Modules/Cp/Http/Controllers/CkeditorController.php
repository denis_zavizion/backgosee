<?php

namespace Modules\Cp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CkeditorController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index()
//    {
//        return view('ckeditor');
//    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
                $file = $request->file('upload');
                $img = time() . '.' . $file->getClientOriginalName();
                $url = '/img/post/';
                $file->move(env('PATH_URL_FRONT') . $url, $img);
            $url = asset(env('URL_FRONT') . $url . $img);
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }

    }
}

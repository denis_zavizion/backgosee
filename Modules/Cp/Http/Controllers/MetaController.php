<?php

namespace Modules\Cp\Http\Controllers;

use App\Permission;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cp\Entities\test1;
use Validator;
use App\meta;
use App\Posts;
use App\Product;
use App\Category;
use App\RolesPermissions;

class MetaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */


    public function page(Request $request)
    {
        $meta = meta::whereNotIn('alias', ['post', 'category', 'product'])->get();
        return view('cp::meta.list', ['meties' => $meta]);
    }
    public function post(Request $request, $p)
    {
        $page = isset($p) && $p >= 1 ? $p : 1;
        $take = 10;
        $skip = ($page*$take-$take);

        $meta = Posts::leftJoin('meta', 'posts.post_id', '=', 'meta.alias_id')->where('status', 1)->orderBy('post_id', 'DESC')
            ->select('meta.*', 'posts.post_id', 'posts.title AS ptitle', 'posts.description AS pdescription')->skip($skip)->take($take)->get();

        $count = Posts::leftJoin('meta', 'posts.post_id', '=', 'meta.alias_id')->where('status', 1)
            ->select('meta.*', 'posts.post_id', 'posts.title AS ptitle', 'posts.description AS pdescription')->count();

        if($count > $take){
            $pages = ceil($count / $take);
        }else{$pages = 1;}
        $end = $pages;
        $k = 3;
        if($p+$k <= $pages){$end = $p+$k;}
        $start = 1;
        if($p-$k > 0){$start = $p-$k;}

        return view('cp::meta.postlist', ['meties' => $meta, 'count'=>$count, 'start'=>$start, 'end'=>$end, 'n'=>$p, 'pages'=>$pages]);
    }
    public function product(Request $request, $p){
        $page = isset($p) && $p >= 1 ? $p : 1;
        $take = 10;
        $skip = ($page*$take-$take);

        $meta = Product::leftJoin('meta', 'product.product_id', '=', 'meta.alias_id')->where('status', 1)->orderBy('product_id', 'DESC')
            ->select('meta.*', 'product.product_id', 'product.title AS ptitle')->skip($skip)->take($take)->get();

        $count = Product::leftJoin('meta', 'product.product_id', '=', 'meta.alias_id')->where('status', 1)
            ->select('meta.*')->count();

        if($count > $take){
            $pages = ceil($count / $take);
        }else{$pages = 1;}
        $end = $pages;
        $k = 3;
        if($p+$k <= $pages){$end = $p+$k;}
        $start = 1;
        if($p-$k > 0){$start = $p-$k;}
//        return view('cp::meta.postlist', ['meties' => $meta, 'count'=>$count, 'start'=>$start, 'end'=>$end, 'n'=>$p, 'pages'=>$pages]);
        return view('cp::meta.productlist', ['meties' => $meta, 'count'=>$count, 'start'=>$start, 'end'=>$end, 'n'=>$p, 'pages'=>$pages]);
    }
    public function category(Request $request)
    {
        $meta = Category::leftJoin('meta', 'category.cat_id', '=', 'meta.alias_id')->where('status', 1)->where('parent_cat_id', 0)
            ->select('meta.*', 'category.cat_id', 'category.title AS ptitle')->get();
        return view('cp::meta.categorylist', ['meties' => $meta]);
    }

    public function pageEdit(Request $request, $id, $n=null)
    {
        $meta = meta::find($id);
        if(in_array($meta->alias, ['post', 'category', 'product'])){
            $link =env('URL_FRONT').'/'.$meta->alias.'/'.$meta->alias_id;
        }else{
            $link =env('URL_FRONT').'/'.$meta->alias;
        }

        if(!isset($meta->title) || !$meta->title || $meta->title == 'not title'){
            if($meta->alias == 'product'){
                $meta->title = Product::where('product_id',$meta->alias_id)->first('title')->title;
            }elseif ($meta->alias == 'category'){
                $meta->title = Category::where('cat_id',$meta->alias_id)->first('title')->title;
            }elseif ($meta->alias == 'post'){
                $meta->title = Posts::where('post_id',$meta->alias_id)->first('title')->title;
            }
            $meta->save();
        }
        return view('cp::meta.edit', ['meta' => $meta, 'link'=>$link, 'n'=>$n]);
    }

    public function save(Request $request, $id)
    {
        $meta = meta::find($id);
        $meta->title=$request->title;
        $meta->keywords=$request->keywords;
        $meta->description=$request->description;
        $meta->save();
        if(in_array($meta->alias, ['post', 'product'])){
            $pag = isset($request->pag) ? $request->pag : 1;
            return redirect('/cp/meta/list/'.$meta->alias.'/'.$pag);
        }elseif($meta->alias == 'category'){
            return redirect('/cp/meta/list/'.$meta->alias);
        }else{
            return redirect('/cp/meta/list/page/');
        }
    }
    public function postCreate(Request $request, $alias, $post_id, $n=null)
    {
        $meta = meta::where('alias', $alias)->where('alias_id', $post_id)->first();
        if (!isset($meta[0]) || !$meta[0]){
            $meta = meta::create([
                'title' => 'not title',
                'description' => null,
                'keywords' => null,
                'alias' => $alias,
                'alias_id' => $post_id
            ]);
            return redirect()->route('cp.meta.edit', [$meta->id, $n]);
        }
        return back();
    }

//    public function categoryCreate(Request $request, $alias, $post_id, $n)
//    {
//        $meta = meta::where('alias', $alias)->where('alias_id', $post_id)->first();
//        if (!isset($meta[0]) || !$meta[0]){
//            $meta = meta::create([
//                'title' => 'not title',
//                'description' => null,
//                'keywords' => null,
//                'alias' => $alias,
//                'alias_id' => $post_id
//            ]);
//            return redirect()->route('cp.meta.edit', [$meta->id, $n]);
//        }
//        return back();
//    }


}

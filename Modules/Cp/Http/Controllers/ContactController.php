<?php


namespace Modules\Cp\Http\Controllers;

use App\contacts;
use Illuminate\Http\Request;

class ContactController
{
    public function index(Request $request, $id=0){
        return view('cp::contacts', ['contacts'=>contacts::where('status', 0)->get()]);
    }

    public function status(Request $request, $contact_id){
        $event = contacts::find($contact_id);
        $event->status = (int)$request->status;
        $event->save();
        return back();
    }
}

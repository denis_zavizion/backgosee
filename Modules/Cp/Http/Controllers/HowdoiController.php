<?php


namespace Modules\Cp\Http\Controllers;

use App\howdoi;
use Illuminate\Http\Request;

class HowdoiController
{
    public function index(Request $request, $id=0){
        return view('cp::howdoi.list', ['howdois'=>howdoi::where('parent_id', $id)->get()]);
    }

    public function removeHowdoi(Request $request, $id){
        howdoi::find($id)->delete();
        return back();
    }

    public function status(Request $request, $id){
        $event = howdoi::find($id);
        $event->status = (int)$request->status;
        $event->save();
        return back();
    }

    public function edit(Request $request, $id){
        return view('cp::howdoi.edit', ['howdoi'=>howdoi::find($id)]);
    }
    public function editSave(Request $request, $id){

        $event = howdoi::find($id);
        $event->title = $request->title;
        $event->description = $request->description;
        $event->save();
        return redirect()->route('howdoi.list', $event->parent_id);
    }

    public function add(Request $request, $id){
        return view('cp::howdoi.add', compact('id'));
    }
    public function addSave(Request $request, $id){
        howdoi::create([
            'title' => $request->title,
            'description' => $request->description,
            'status' => 0,
            'parent_id' => isset($id) ? (int)$id : 0
        ]);
        return redirect()->route('howdoi.list', $id);
    }
}

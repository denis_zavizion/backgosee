<?php


namespace Modules\Cp\Http\Controllers;

use App\Account;
use App\followers;
use Illuminate\Http\Request;

class DealerController
{
    public function index(Request $request, $account=1, $apply = 1){
        return view('cp::dealer', ['dealers'=>Account::where('account', $account)->where('apply', $apply)->get()]);
    }

    public function status(Request $request, $id){
        $event = Account::find($id);
        $event->account = (int)$request->account;
        $event->apply = 0;
        $event->save();
        $url = env('URL_FRONT').'/account/'.$id.'/allads';

        if($request->account == 2){
            $src_url = "<a href='".$url."/v17'>".$url."</a>";
            followers::create([
                'to_user_id' => $id,
                'from_user_id' => 1,
                'description' => "You have now been approved as a dealer on gosee.ie, check out your new page ".$src_url
            ]);
        }else{
            $src_url = "<a href='".$url."/v16'>".$url."</a>";
            followers::create([
                'to_user_id' => $id,
                'from_user_id' => 1,
                'description' => "You have now been approved as a private on gosee.ie, check out your new page ".$src_url
            ]);
        }


        return back();
    }
}

<?php


namespace Modules\Cp\Http\Controllers;

use App\term_meta;
use Illuminate\Http\Request;

class PageController
{
    public function index(Request $request){
        return view('cp::page.list', ['pages'=>term_meta::all()]);
    }

    public function edit(Request $request, $term_id){
        return view('cp::page.edit', ['page'=>term_meta::where('term_id', $term_id)->first()]);
    }
    public function editSave(Request $request, $term_id){
        $page = term_meta::where('term_id', $term_id)
            ->update(['title' => $request->title, 'val'=>$request->val]);
        return redirect()->route('page.list');
    }


}

<?php


namespace Modules\Cp\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;

class EventController
{
    public function index(Request $request){
        return view('cp::events.list', ['events'=>Events::all()]);
    }

    public function removeEvent(Request $request, $id){
        Events::find($id)->delete();
        return back();
    }

    public function status(Request $request, $id){
        $event = Events::find($id);
        $event->status = (int)$request->status;
        $event->save();
        return back();
    }
}

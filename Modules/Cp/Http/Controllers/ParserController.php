<?php


namespace Modules\Cp\Http\Controllers;
use App\term_meta;
use App\m6motors;
use App\Product;
use App\Category;
use App\Options;
use App\SubOptions;
use App\ProductOption;
use App\product_suboption;
use Illuminate\Http\Request;

class ParserController
{
    public function index(Request $request){
        return view('cp::parser.index', ['pages'=>term_meta::all()]);
    }
    public function setting(Request $request){
        return view('cp::parser.setting', ['pages'=>term_meta::all()]);
    }

    public function nonceM6motors(Request $request){

        $this->update_term_meta('m6motors', $request->nonce);
        return redirect()->route('parser.m6motors');
    }

    public function m6motors(Request $request){
        $resultsApi = json_decode($this->m6motorsApi($request));
        if (isset($resultsApi->data->cars) && $resultsApi->data->cars){
            $cars = $resultsApi->data->cars;

            foreach ($cars as $result){
                $res = m6motors::where('api_id',$result->id)->get();
                if(!isset($res[0]) || !$res[0]){
                    m6motors::create(array(
                        'api_id'  => $result->id,
                        'status' => 'pending',
                        'mass' => json_encode($result)
                    ));
                }
            }
            return redirect()->route('parser.m6motors.result');
        }
        return view('cp::parser.m6motors.setting');
    }

    public function m6motorsResult(Request $request){
        $m6motors = m6motors::where('status', 'pending')->limit(10)->get();
        $added = m6motors::where('m6motors.status', 'added')->orWhere('m6motors.status', 1)
            ->leftJoin('product', 'product.product_id', '=', 'm6motors.product_id')
            ->select('product.title','m6motors.*')
            ->get();

        return view('cp::parser.m6motors.m6motors', ['m6motors'=>$m6motors, 'added'=>$added]);
    }

    public static function mass($api_id, $data){

        $results = json_decode($data);
        $body = category::where('parent_cat_id', 1)->where('title', $results->specs->body)->first();
        $cat_id = $body['cat_id'];
        if(!isset($cat_id) && !$cat_id){$cat_id = 13;}

        $user_id = env('PARSER_M6_USER_ID');
        $product = Product::create(array(
            'title'  => $results->name,
            'budget' => $results->price,
            'year' => $results->specs->year,
            'cat_id' => $cat_id,
            'user_id' =>$user_id,
            'img' => '',
            'gallery' => '[]',
            'name' => '',
            'country' => '',
            'phone' => '',
            'town' => '',
            'allow_messenger' => 0,
            'allow_phone' => 0,
            'allow_receipt' => 0,
            'milage' => preg_replace("/[^0-9]/", '', $results->specs->mileage->kms),
            'status' => 0,
            'description' => $results->blurb
        ));

        //make model
        $make = Options::where('opt_name', $results->make)->where('filter_id', 1)->where('status', 1)->first();
        if(isset($make->opt_id) && $make->opt_id){
            //marka
            ProductOption::create([
                'product_id' => $product->product_id,
                'opt_id' => $make->opt_id
            ]);
            //model
            $sub_option = SubOptions::where('sub_opt_title', $results->model)->where('opt_id', $make->opt_id)->where('status', 1)->first();
            if(isset($sub_option) && $sub_option){
                product_suboption::create([
                    'product_id' => $product->product_id,
                    'sub_option_id' => $sub_option->sub_option_id
                ]);
            }
        }

        $fuel = Options::where('opt_name', $results->specs->fuel)->where('filter_id', 6)->where('status', 1)->first();
        if(isset($fuel) && $fuel){
            ProductOption::create([
                'product_id' => $product->product_id,
                'opt_id' => $fuel->opt_id
            ]);
        }


        $transmission = Options::where('opt_name', $results->specs->transmission)->where('filter_id', 7)->where('status', 1)->first();

        if(isset($transmission) && $transmission){
            ProductOption::create([
                'product_id' => $product->product_id,
                'opt_id' => $transmission->opt_id
            ]);
        }

        $colour = Options::where('opt_name', $results->specs->colour)->where('filter_id', 8)->where('status', 1)->first();
        if(isset($colour) && $colour){
            ProductOption::create([
                'product_id' => $product->product_id,
                'opt_id' => $colour->opt_id
            ]);
        }

        $doors = Options::where('opt_name', $results->specs->doors)->where('filter_id', 9)->where('status', 1)->first();
        if(isset($doors) && $doors){
            ProductOption::create([
                'product_id' => $product->product_id,
                'opt_id' => $doors->opt_id
            ]);
        }

        //Загрузка фото
        $urls = [];
        foreach ($results->images->large as $key=>$image){
            if($key == 0){$url = basename($image);}else{$urls[] = basename($image);}
            $path = env('PATH_URL_FRONT').'/img/product/'.$user_id.'/'.$product->product_id;
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            try {
                $ch=curl_init($image);
                $fp = fopen($path.'/'.basename($image), 'wb');
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                curl_close($ch);
                fclose($fp);
//
            }catch (Exception $e) {
                echo $e->getMessage();
                Product::where('product_id', $product->product_id)->update(['status' => 0]);
            }

//            try {
//                if(empty(file_get_contents($image, true))){
//                    throw new Exception("failed to open stream ", 1);
//                }else{
//                    file_put_contents($path.'/'.basename($image), file_get_contents($image));
//                }
//            }catch (Exception $e) {
//                return 'The file was not found';
//            }
        }


        Product::where('product_id', $product->product_id)->update(['gallery' => json_encode($urls), 'img' => $url]);
        $m6motors = m6motors::where('api_id', $api_id)->update(['status' => 'added', 'product_id'=>$product->product_id]);


        if(isset($results) && $results)
            return view('cp::parser.m6motors.pending', ['product_id'=>$product->product_id, 'name'=>$results->name]);

        return 'No result';
    }
    public function status(Request $request, $id){
        $m6motors = m6motors::find($id);

        if(isset($m6motors->status) && $m6motors->status == 1){
            $m6motors->status = 'added';
            Product::where('product_id', $m6motors->product_id)->update(['status' => 0]);
        }else{
            $m6motors->status = 1;
            Product::where('product_id', $m6motors->product_id)->update(['status' => 1]);
        }
        $m6motors->save();

        return back();
    }
    private function m6motorsApi($request){
        $nonce = $this->get_term_meta('m6motors');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://www.m6motors.ie/wp-admin/admin-ajax.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'action=get_advert_ids&nonce='.$nonce.'&makeId=&modelId=&priceLowerId=1&priceUpperId=9&priceLower=0&priceUpper=1000000&mileageLowerId=1&mileageUpperId=14&mileageLower=0&mileageUpper=1000000&yearLowerId=214&yearUpperId=221&boundedMinYear=2012&yearLower=2012&boundedMaxYear=2019&yearUpper=2019&engineLowerId=1&engineUpperId=85&engineLower=0.0&engineUpper=1000000.0&distanceKms=&financeOnly=&sortById=10&loadCount=5&initialCall=1',
            CURLOPT_HTTPHEADER => array(
                'Connection: keep-alive',
                'Pragma: no-cache',
                'Cache-Control: no-cache',
                'Accept: application/json, text/javascript, */*; q=0.01',
                'Origin: https://www.m6motors.ie',
                'X-Requested-With: XMLHttpRequest',
                'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36',
                'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
                'Sec-Fetch-Site: same-origin',
                'Sec-Fetch-Mode: cors',
                'Referer: https://www.m6motors.ie/used-cars/',
                'Accept-Encoding: gzip, deflate, br',
                'Accept-Language: ru,uk;q=0.9,ru-RU;q=0.8,en;q=0.7',
                'Cookie: PHPSESSID=o3bulud30qf4uff5o4sgalccp3; _ga=GA1.2.1339222193.1611693510; _gid=GA1.2.1174883308.1612044198; _gat=1; PHPSESSID=p5chttrpvb2fg17fqknqveslr1'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
    function get_term_meta($key){
        return term_meta::where('key', $key)->first('val')->val;
    }

    function update_term_meta($key, $val){
        $flag = term_meta::where('key', '=', $key)->first();
        if(isset($flag) && $flag){term_meta::where('key', $key)->update(['val' => $val]);}else{term_meta::create(['key' => $key,'val' => $val]);}
        return true;
    }

}

<?php
Route::group(['prefix' => 'events', 'middleware' => 'permission:view_events'], function () {
    Route::get('/', 'EventController@index')->name('events.list');
    Route::get('/remove/{id}', 'EventController@removeEvent')->name('events.remove')->middleware('permission:edit_events');
    Route::get('/status/{id}', 'EventController@status')->name('events.status')->middleware('permission:edit_events');
});

<?php
Route::group(['prefix' => 'user', 'middleware' => 'permission:veiw_panel'], function () {
    Route::get('/{id}', 'CpController@user')->name('cp.user');
    Route::get('/{id}/role/{role_slug}', 'CpController@createRoleUser')->middleware('permission:edit_permission')->name('cp.user.role');
    Route::get('/{id}/role/{role_slug}/remove', 'CpController@removeRoleUser')->middleware('permission:edit_permission')->name('cp.user.role.remove');
    Route::get('/{id}/permission/{permission_slug}', 'CpController@createPermissionUser')->middleware('permission:edit_permission')->name('cp.user.permission');
    Route::get('/{id}/permission/{permission_slug}/remove', 'CpController@removePermissionUser')->middleware('permission:edit_permission')->name('cp.user.permission.remove');
    Route::get('/{id}/remove', 'CpController@removeUser')->middleware('permission:add_remov_user')->name('cp.user.remove');
});
Route::get('create/user', 'CpController@createUser')->middleware('permission:add_remov_user')->name('cp.user.add');
Route::post('create/user', 'CpController@createUser')->middleware('permission:add_remov_user')->name('cp.user.add.post');
Route::get('edit/user/{id}', 'CpController@editUser')->middleware('permission:edit_user')->name('cp.user.edit');
Route::post('edit/user/{id}', 'CpController@editUser')->middleware('permission:edit_user')->name('cp.user.edit');

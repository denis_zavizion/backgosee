<?php
Route::group(['prefix' => 'filter', 'middleware' => 'permission:veiw_panel'], function () {
    include 'filter/category.php';
    include 'filter/filter.php';
});

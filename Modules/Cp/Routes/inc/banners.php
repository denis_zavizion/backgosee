<?php

//use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'banners', 'middleware' => 'permission:view_banner'], function () {
    Route::get('/', 'BannerController@index')->name('banner.list');

    Route::get('/edit/{id}', 'BannerController@edit')->name('banner.list.edit')->middleware('permission:edit_banner');
    Route::post('/edit/{id}/save', 'BannerController@editSave')->name('banner.list.save')->middleware('permission:edit_banner');
});

Route::group(['prefix' => 'sidebar', 'middleware' => 'permission:view_banner'], function () {
    Route::get('/list/{page}/{position}', 'BannerController@sidebar')->name('banner.sidebar');

    Route::get('/banner/add/{page}/{position}', 'BannerController@addSidebarBanner')->name('banner.sidebar.banner.add')->middleware('permission:edit_banner');
    Route::get('/audio/add/{page}/{position}', 'BannerController@addSidebarAudio')->name('banner.sidebar.audio.add')->middleware('permission:edit_banner');
    Route::get('/product/add/{page}/{position}', 'BannerController@addSidebarProduct')->name('banner.sidebar.product.add')->middleware('permission:edit_banner');

    Route::post('/save/{page}/{position}', 'BannerController@saveSidebar')->name('banner.sidebar.save')->middleware('permission:edit_banner');
    Route::get('/remove/{id}', 'BannerController@removeSidebar')->name('banner.sidebar.remove')->middleware('permission:edit_banner');
});

<?php
Route::group(['prefix' => 'page'], function () {
    Route::get('/', 'PageController@index')->name('page.list');
    Route::get('/edit/{term_id}', 'PageController@edit')->name('page.edit');
    Route::post('/save/{term_id}', 'PageController@editSave')->name('page.edit.save');
});

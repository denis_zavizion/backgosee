<?php
Route::group(['prefix' => 'contacts'], function () {
    Route::get('/', 'ContactController@index')->name('contact.list');
    Route::get('/status/{contact_id}', 'ContactController@status')->name('contact.status');
});

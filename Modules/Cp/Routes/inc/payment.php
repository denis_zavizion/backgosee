<?php
Route::group(['prefix' => 'payment', 'middleware' => 'permission:view_payment'], function () {
    Route::get('/check', 'PaymentController@check')->name('payment.check');
    Route::get('/pending', 'PaymentController@pending')->name('payment.pending');
    Route::get('/successful', 'PaymentController@successful')->name('payment.successful');
    Route::get('/archive', 'PaymentController@archive')->name('payment.archive');

    Route::group(['prefix' => 'save', 'middleware' => 'permission:edit_payment'], function () {
        Route::get('/archive/{product_id}', 'PaymentController@archiveSave')->name('payment.archive.save');
        // История оплаченых
        Route::get('/successful/{product_id}', 'PaymentController@successfulSave')->name('payment.successful.save');
        // Отключить Status Product
        Route::get('/stop/{product_id}', 'PaymentController@stopSave')->name('payment.stop.save')->middleware('permission:stop_payment');
    });
});




<?php
Route::group(['prefix' => 'role', 'middleware' => 'permission:veiw_panel'], function () {
    Route::get('/create', 'RolesController@createRole')->middleware('permission:update_permission_role')->name('cp.role.create');
    Route::post('/create', 'RolesController@createRole')->middleware('permission:update_permission_role')->name('cp.role.create');

    Route::get('/list', 'RolesController@listRole')->middleware('permission:update_permission_role')->name('cp.role.list');
    Route::get('/remove/{role_id}', 'RolesController@removeRole')->middleware('permission:update_permission_role')->name('cp.role.remove');

    Route::get('/update/{role_id}', 'RolesController@updateRole')->middleware('permission:update_permission_role')->name('cp.role.update');
    Route::get('/update/add/{role_id}/{permission_id}', 'RolesController@updateRoleAdd')->middleware('permission:update_permission_role')->name('cp.role.update.add');
    Route::get('/update/remove/{role_id}/{permission_id}', 'RolesController@updateRoleRemove')->middleware('permission:update_permission_role')->name('cp.role.update.remove');
});

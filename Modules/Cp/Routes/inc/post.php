<?php
Route::group(['prefix' => 'post', 'middleware' => 'permission:veiw_panel'], function () {
    Route::get('/', 'PostsController@index')->name('posts');
    Route::get('/add', 'PostsController@create')->name('post_add');
    Route::get('/edit/{post_id}', 'PostsController@edit')->name('post_edit');
    Route::post('/edit/{post_id}/save', 'PostsController@save')->name('post_edit_save');
    Route::post('/add/save', 'PostsController@addPostSave')->name('post_add_save');
    Route::get('/remove/{id}', 'PostsController@remove')->name('remove');
    Route::get('/status/{id}', 'PostsController@statusPost')->name('post_status');
});

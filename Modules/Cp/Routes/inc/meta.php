<?php
Route::group(['prefix' => 'meta'], function () {
    Route::get('/list/page', 'MetaController@page')->name('cp.meta.page.list');
    Route::get('/list/post/{p}', 'MetaController@post')->name('cp.meta.post.list');
    Route::get('/list/category', 'MetaController@category')->name('cp.meta.category.list');
    Route::get('/list/product/{p}', 'MetaController@product')->name('cp.meta.product.list');

    Route::get('/edit/page/{id}/{n?}', 'MetaController@pageEdit')->name('cp.meta.edit');
    Route::get('/create/alias/{alias}/{post_id}/{n?}', 'MetaController@postCreate')->name('cp.meta.create.post');

    Route::post('/save/{id}', 'MetaController@save')->name('cp.meta.save');
});

<?php
Route::group(['prefix' => 'dealer'], function () {
    Route::get('/{account?}/{apply?}', 'DealerController@index')->name('dealer.list');
    Route::post('/status/{id}', 'DealerController@status')->name('dealer.status');
});

<?php
Route::group(['prefix' => 'howdoi', 'middleware' => 'permission:view_howdoi'], function () {
    Route::get('/{id?}', 'HowdoiController@index')->name('howdoi.list');
    Route::get('/remove/{id}', 'HowdoiController@removeHowdoi')->name('howdoi.remove')->middleware('permission:edit_howdoi');
    Route::get('/status/{id}', 'HowdoiController@status')->name('howdoi.status')->middleware('permission:edit_howdoi');

    Route::get('/edit/{id}', 'HowdoiController@edit')->name('howdoi.edit')->middleware('permission:edit_howdoi');
    Route::post('/edit/{id}/save', 'HowdoiController@editSave')->name('howdoi.edit.save')->middleware('permission:edit_howdoi');

    Route::get('/add/{id?}', 'HowdoiController@add')->name('howdoi.add')->middleware('permission:edit_howdoi');
    Route::post('/add/{id}/save', 'HowdoiController@addSave')->name('howdoi.add.save')->middleware('permission:edit_howdoi');
});

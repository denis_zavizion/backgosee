<?php
Route::group(['prefix' => 'category', 'middleware' => 'permission:view_category'], function () {
    Route::get('/', 'filter\CategoryController@index')->name('filter.category');
    Route::get('/parent/list/{id}', 'filter\CategoryController@parent')->name('filter.parent');

    Route::get('/parent/add/', 'filter\CategoryController@addParent')->name('filter.category.add')->middleware('permission:edit_category');
    Route::post('/parent/add/save/', 'filter\CategoryController@addSaveParent')->name('filter.category.add.save')->middleware('permission:edit_category');
    Route::get('/parent/status/{id}', 'filter\CategoryController@statusSaveParent')->name('filter.category.cat_status')->middleware('permission:edit_category');

    Route::get('/auto/list/report/product/{id}', 'filter\CategoryController@getReport')->name('report')->middleware('permission:edit_category');
    Route::get('/auto/list/filter/reports', 'filter\CategoryController@filterReport')->name('filter.report')->middleware('permission:edit_category');
    Route::get('/auto/list/report/product/status/{id}', 'filter\CategoryController@reportStatus')->name('filter.parent.report')->middleware('permission:edit_category');
    Route::get('/auto/list/product/{id}', 'filter\CategoryController@listAuto')->name('filter.parent.auto')->middleware('permission:edit_category');
    Route::get('/auto/list/status/{id}', 'filter\CategoryController@listAutoStatus')->name('filter.parent.auto.status')->middleware('permission:edit_category');
    Route::get('/auto/list/special/{id}', 'filter\CategoryController@listAutoSpecial')->name('filter.parent.auto.special')->middleware('permission:edit_category');

    Route::get('/parent/edit/{id}', 'filter\CategoryController@editParent')->name('filter.parent.edit')->middleware('permission:edit_category');
    Route::post('/parent/save/{cat_id}', 'filter\CategoryController@saveParent')->name('filter.parent.save')->middleware('permission:edit_category');

    Route::get('/parent/delete/{cat_id}', 'filter\CategoryController@delete')->name('filter.parent.delete');
});


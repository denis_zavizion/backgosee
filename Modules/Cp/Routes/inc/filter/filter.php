<?php
Route::group(['prefix' => 'name', 'middleware' => 'permission:veiw_panel_filter'], function () {
    Route::get('/list/{id}', 'filter\FiltersController@list')->name('filter.list');
    Route::group(['prefix' => 'option', 'middleware' => 'permission:veiw_panel_options'], function () {
        Route::get('/{filter_id}', 'option\OptionsController@index')->name('option');
        Route::get('/edit/{opt_id}', 'option\OptionsController@edit')->name('option.edit')->middleware('permission:edit_options');
        Route::get('/add/{filter_id}', 'option\OptionsController@add')->name('option.add')->middleware('permission:edit_options');
        Route::post('/save/{opt_id}/{filter_id}', 'option\OptionsController@save')->name('option.save')->middleware('permission:edit_options');
        Route::post('/addsave/{filter_id}', 'option\OptionsController@addSave')->name('option.add.save')->middleware('permission:edit_options');
        Route::get('/delete/{opt_id}', 'option\OptionsController@delete')->name('option.delete')->middleware('permission:edit_options');
        Route::get('/status/{opt_id}', 'option\OptionsController@status')->name('option.status')->middleware('permission:edit_options');

        Route::group(['prefix' => 'suboptions', 'middleware' => 'permission:veiw_panel_options'], function () {
            Route::get('/{opt_id}', 'option\SubOptionsController@index')->name('suboption');
            Route::get('/edit/{sub_option_id}', 'option\SubOptionsController@edit')->name('sub.option.edit')->middleware('permission:edit_options');
            Route::get('/add/{opt_id}', 'option\SubOptionsController@add')->name('sub.option.add')->middleware('permission:edit_options');
            Route::post('/save/{opt_id}/{sub_option_id}', 'option\SubOptionsController@save')->name('sub.option.save')->middleware('permission:edit_options');
            Route::post('/addsave/{opt_id}', 'option\SubOptionsController@addSave')->name('sub.option.add.save')->middleware('permission:edit_options');
            Route::get('/delete/{sub_option_id}', 'option\SubOptionsController@delete')->name('sub.option.delete')->middleware('permission:edit_options');
            Route::get('/status/{sub_option_id}', 'option\SubOptionsController@status')->name('sub.option.status')->middleware('permission:edit_options');
        });

    });
    Route::get('/list/status/{filter_id}', 'filter\FiltersController@status')->name('category.filter.status')->middleware('permission:edit_filter');
    Route::get('/list/edit/{filter_id}', 'filter\FiltersController@edit')->name('category.filter.edit')->middleware('permission:edit_filter');
    Route::post('/list/save/{filter_id}', 'filter\FiltersController@save')->name('category.filter.save')->middleware('permission:edit_filter');
    Route::get('/list/add/{cat_id}', 'filter\FiltersController@add')->name('category.filter.add')->middleware('permission:edit_filter');
    Route::post('/list/add/save/{cat_id}', 'filter\FiltersController@addSave')->name('category.filter.add.save')->middleware('permission:edit_filter');
    Route::get('/list/add/delete/{filter_id}', 'filter\FiltersController@delete')->name('category.filter.delete')->middleware('permission:edit_filter');
});

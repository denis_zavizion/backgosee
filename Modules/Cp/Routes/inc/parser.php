<?php
Route::group(['prefix' => 'parser'], function () {
    Route::get('/', 'ParserController@index')->name('parser.index');
    Route::get('/setting', 'ParserController@setting')->name('parser.setting');



    Route::post('/m6motors/nonce', 'ParserController@nonceM6motors')->name('parser.m6motors.nonce');
    Route::get('/m6motors', 'ParserController@m6motors')->name('parser.m6motors');
    Route::get('/m6motors/status/{id}', 'ParserController@status')->name('m6motors.status');

    Route::get('/m6motors-result', 'ParserController@m6motorsResult')->name('parser.m6motors.result');

















});

<?php

namespace Modules\Cp\Routes;
use Modules\Cp\Routes\inc\index;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('ckeditor', 'CkeditorController@index');


Route::group(['prefix' => 'cp', 'middleware' => 'permission:veiw_panel'], function () {
    Route::post('ckeditor/upload', 'CkeditorController@upload')->name('ckeditor.upload');
    Route::get('/dashboard', 'CpController@index')->name('dashboard');
    // маршруты поста
    include 'inc/post.php';
    // маршруты Юзера
    include 'inc/user.php';
    // маршруты Роута
    include 'inc/role.php';
    // маршруты Фильтра
    include 'inc/filter.php';
    // payment
    include 'inc/payment.php';
    // banners
    include 'inc/banners.php';
    // events
    include 'inc/events.php';
    // howdoi
    include 'inc/howdoi.php';
    // contact
    include 'inc/contact.php';
    // page
    include 'inc/page.php';
    // dealer
    include 'inc/dealer.php';
    // parser
    include 'inc/parser.php';
    // seo
    include 'inc/meta.php';
});
